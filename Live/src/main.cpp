#include <iostream>
#include "square/CPoint.h"
#include "square/CSquare.h"

void test() {
    //CPoint x;
    CPoint a(1, 1), b(2, 2);
    CPoint c(3, 3), d(a);
    std::cout << a << b << c << d << std::endl; //(1, 1) (2, 2) (3, 3) (1, 1)

    c=b;
    c.setX(10); c.setY(10);
    a.setX(20); a.setY(20);
    std::cout << a << b << c << d << std::endl; //(20, 20) (2, 2) (10, 10) (1, 1)

    a = b + c;

    std::cout << a << b << c << std::endl; // (12, 12) (2, 2) (10, 10)
}

void testSquare() {
    CSquare p1(0, 0, 4, 5);
    CSquare p2(1, 2, 8, 3);
    CSquare p3;
    p3 = p1 + p2;
    std::cout << p3 << std::endl; //[(0, 0) (8, 5)]
}

int main() {
    test();
    std::cout << std::endl;
    testSquare();
    return 0;
}
