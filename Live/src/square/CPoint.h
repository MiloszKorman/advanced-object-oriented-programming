//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef LIVE_CPOINT_H
#define LIVE_CPOINT_H

#include <iostream>


class CPoint {
public:
    CPoint(int x, int y) {
        px = new int(x);
        py = new int(y);
    }

    CPoint(CPoint &point) {
        px = new int(*point.px);
        py = new int(*point.py);
    }

    ~CPoint() {
        delete px;
        delete py;
    }

    void operator=(const CPoint &otherPoint);

    CPoint operator+(CPoint &otherPoint);

    void setX(int nX);

    void setY(int nY);

    int getX();

    int getY();

    friend std::ostream &operator<<(std::ostream &os, const CPoint &point);

private:
    int *px;
    int *py;
};


#endif //LIVE_CPOINT_H
