//
// Created by Miłosz Korman on 22.11.18.
//

#include "CSquare.h"

std::ostream &operator<<(std::ostream &os, const CSquare &square) {
    os << "[edge1: " << square.edge1 << " edge2: " << square.edge2 << "]";
    return os;
}

void CSquare::operator=(const CSquare &otherSquare) {
    this->edge1 = otherSquare.edge1;
    this->edge2 = otherSquare.edge2;
}

CSquare CSquare::operator+(CSquare &otherSquare) {
    int leftX = findMin(
            findMin(this->edge1.getX(), this->edge2.getX()),
            findMin(otherSquare.edge1.getX(), otherSquare.edge2.getX())
            );
    int leftY = findMin(
            findMin(this->edge1.getY(), this->edge2.getY()),
            findMin(otherSquare.edge1.getY(), otherSquare.edge2.getY())
    );

    int rightX = findMax(
            findMax(this->edge1.getX(), this->edge2.getX()),
            findMax(otherSquare.edge1.getX(), otherSquare.edge2.getX())
    );
    int rightY = findMax(
            findMax(this->edge1.getY(), this->edge2.getY()),
            findMax(otherSquare.edge1.getY(), otherSquare.edge2.getY())
    );



    CSquare toReturn(leftX, leftY, rightX, rightY);
    return toReturn;
}

int CSquare::findMin(int a, int b) {
    if (a < b) return a;
    else return b;
}

int CSquare::findMax(int a, int b) {
    if (a > b) return a;
    else return b;
}
