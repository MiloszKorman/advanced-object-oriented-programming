//
// Created by Miłosz Korman on 22.11.18.
//

#include "CPoint.h"

std::ostream &operator<<(std::ostream &os, const CPoint &point) {
    os << "(px: " << *point.px << " py: " << *point.py << ")";
    return os;
}

void CPoint::operator=(const CPoint &otherPoint) {
    *px = *otherPoint.px;
    *py = *otherPoint.py;
}

CPoint CPoint::operator+(CPoint &otherPoint) {
    CPoint toReturn(*px + *otherPoint.px, *py + *otherPoint.py);
    return toReturn;
}

void CPoint::setX(int nX) {
    *px = nX;
}

void CPoint::setY(int nY) {
    *py = nY;
}

int CPoint::getX() {
    return *px;
}

int CPoint::getY() {
    return *py;
}
