//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef LIVE_CSQUARE_H
#define LIVE_CSQUARE_H


#include <ostream>
#include "CPoint.h"

class CSquare {
public:
    CSquare() : edge1(0, 0), edge2(0, 0) {}
    CSquare(CPoint &edge1, CPoint &edge2) : edge1(edge1), edge2(edge2) {}
    CSquare(int x1, int y1, int x2, int y2) : edge1(x1, y1), edge2(x2, y2) {}

    friend std::ostream &operator<<(std::ostream &os, const CSquare &square);

    void operator=(const CSquare &otherSquare);
    CSquare operator+(CSquare &otherSquare);

private:
    CPoint edge1;
    CPoint edge2;

    int findMin(int a, int b);
    int findMax(int a, int b);
};


#endif //LIVE_CSQUARE_H
