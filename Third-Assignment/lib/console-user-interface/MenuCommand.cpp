//
// Created by Miłosz Korman on 21.10.18.
//

#include <iostream>
#include "MenuCommand.h"

MenuCommand::MenuCommand() : MenuItem() {
    this->helpText = "Default help text";
}

MenuCommand::MenuCommand(const std::string command, const std::string name) : MenuItem(command, name) {
    this->helpText = "Default help text";
}

MenuCommand::MenuCommand(const std::string command, const std::string name, const std::string help) : MenuItem(command, name) {
    this->helpText = help;
}

MenuCommand::MenuCommand(const std::string command, const std::string name, Command *givenCommand)
        : MenuItem(command, name) {
    changeCommand(givenCommand);
    this->helpText = "Default help text";
}

MenuCommand::MenuCommand(const std::string command, const std::string name, Command *givenCommand,
        const std::string helpText)
            : MenuCommand(command, name, givenCommand) {
    this->helpText = helpText;
}

MenuCommand::~MenuCommand() {
    if (command)
        delete command;
}

void MenuCommand::Run() {
    if (command)
        command->RunCommand();
    else
        std::cout << NULL_COMMAND << std::endl;
}

void MenuCommand::changeCommand(Command *givenCommand) {
    this->command = givenCommand;
}

std::string MenuCommand::getHelp() {
    return helpText;
}
