//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_MENUCOMMAND_H
#define SECOND_ASSIGNMENT_MENUCOMMAND_H


#define NULL_COMMAND "Empty command"

#include "MenuItem.h"
#include "Command.h"

class MenuCommand : public MenuItem {
public:
    MenuCommand();

    MenuCommand(const std::string command, const std::string name);

    MenuCommand(const std::string command, const std::string name, const std::string help);

    MenuCommand(const std::string command, const std::string name, Command *givenCommand);

    MenuCommand(const std::string command, const std::string name, Command *givenCommand, const std::string helpText);

    ~MenuCommand() override;

    void Run() override;

    void changeCommand(Command *givenCommand);

    std::string getHelp();

private:
    Command *command;

    std::string helpText;
};


#endif //SECOND_ASSIGNMENT_MENUCOMMAND_H
