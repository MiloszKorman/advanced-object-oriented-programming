//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_MENUITEM_H
#define SECOND_ASSIGNMENT_MENUITEM_H

#define DEFAULT_COMMAND "Default Command"
#define DEFAULT_NAME "Default Name"
#define MENU_ITEM_DESTRUCTOR "### Deleting MenuItem: "
#define TRIPLE_HASH " ###"


#include <string>

class MenuItem {
public:
    MenuItem();

    MenuItem(const std::string command, const std::string name);

    virtual ~MenuItem();

    virtual void Run() = 0;

    std::string getCommand();

    std::string getName();

    std::string toString();

protected:
    std::string command;
    std::string name;
};

#endif //SECOND_ASSIGNMENT_MENUITEM_H
