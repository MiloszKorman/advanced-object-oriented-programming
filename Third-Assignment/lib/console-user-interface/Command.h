//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_COMMAND_H
#define SECOND_ASSIGNMENT_COMMAND_H

#define DEFAULT_COMMAND "Default command"

class Command {
public:
    virtual void RunCommand();
};

#endif //SECOND_ASSIGNMENT_COMMAND_H
