//
// Created by Miłosz Korman on 21.10.18.
//

#include <iostream>
#include <algorithm>
#include "Menu.h"
#include "../utils/Utils.h"
#include "../utils/MenuAnalyser.h"
#include "../utils/MenuSerializer.h"

Menu::Menu() : MenuItem() {
}

Menu::Menu(const std::string command, const std::string name) : MenuItem(command, name) {
}

Menu::Menu(const std::string command, const std::string name, MenuAnalyser *analyser) : MenuItem(command, name) {
    this->analyser = analyser;
}

Menu::~Menu() {
    for (int i = 0; i < items.size(); ++i)
        delete items[i];
    items.clear();
}

Menu &Menu::operator=(const Menu &copyOther) {
    this->name = copyOther.name;
    this->command = copyOther.command;

    for (int i = 0; i < this->items.size() ; ++i) {
        delete items[i];
    }

    items.clear();

    for (int i = 0; i < copyOther.items.size() ; ++i) {
        items.push_back(copyOther.items[i]);
    }

    return *this;
}

void Menu::Run() {

    std::string userChoice;
    bool correct = false;

    do {
        std::cout << NEW_LINES << this->toString() << std::endl;

        std::cout << INTRODUCE_OPTIONS << std::endl;
        for (int i = 0; i < items.size(); ++i)
            std::cout << items[i]->toString() << std::endl;

        std::cout << SAVE_OPTION << "\n"
                  << LOAD_OPTION << std::endl;

        std::cout << GET_USERS_CHOICE;

        userChoice = Utils::getString();
        transform(userChoice.begin(), userChoice.end(), userChoice.begin(), ::tolower);
        if (userChoice.length() > MIN_HELP_LENGTH && userChoice.substr(0, MIN_HELP_LENGTH) == HELP_COMMAND_START) {

            correct = showCommandHelp(userChoice.substr(MIN_HELP_LENGTH, userChoice.length()));

        } else if (userChoice.length() > MIN_SEARCH_LENGTH &&
                   userChoice.substr(0, MIN_SEARCH_LENGTH) == SEARCH_COMMAND_START) {

            analyser->searchForCommandNPrint(NULL, userChoice.substr(MIN_SEARCH_LENGTH, userChoice.length()), "",
                                             correct);

        } else if (userChoice.length() > MIN_SERIALIZATION_LENGTH &&
                   (userChoice.substr(0, MIN_SERIALIZATION_LENGTH) == SAVE_COMMAND ||
                    userChoice.substr(0, MIN_SERIALIZATION_LENGTH) == LOAD_COMMAND)) {

            std::string serializationChoice = userChoice.substr(0, MIN_SERIALIZATION_LENGTH);
            std::string fileName = userChoice.substr(MIN_SERIALIZATION_LENGTH, userChoice.length());

            if (serializationChoice == SAVE_COMMAND) {
                correct = MenuSerializer::serializeToFile(this, fileName);
            } else {
                correct = MenuSerializer::deserializeFromFile(this, fileName);
            }

        } else {
            correct = chooseCommandNRun(userChoice);
        }

        if (!correct && userChoice != END_RUN)
            std::cout << INCORRECT_INPUT << std::endl;

    } while (userChoice != END_RUN);
}

void Menu::addItem(MenuItem *item) {
    bool unique = true;
    for (int i = 0; i < items.size(); ++i)
        if (item->getName() == items[i]->getName() || item->getCommand() == items[i]->getCommand())
            unique = false;

    if (unique)
        items.push_back(item);
}

void Menu::removeItem(std::string itemName) {
    for (int i = 0; i < items.size(); ++i)
        if (itemName == items[i]->getCommand())
            items.erase(items.begin() + i);
}

bool Menu::chooseCommandNRun(const std::string &commandName) {
    for (int i = 0; i < items.size(); ++i)
        if (commandName == items[i]->getCommand()) {
            items[i]->Run();
            return true;
        }

    return false;
}

bool Menu::showCommandHelp(const std::string &commandName) {
    for (int i = 0; i < items.size(); ++i)
        if (commandName == items[i]->getCommand())
            if (MenuCommand *menuCommand = dynamic_cast<MenuCommand *>(items[i])) {
                std::cout << "\n" << menuCommand->getHelp() << std::endl;
                return true;
            }

    return false;
}
