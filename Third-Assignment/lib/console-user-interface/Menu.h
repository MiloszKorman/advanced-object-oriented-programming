//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_MENU_H
#define SECOND_ASSIGNMENT_MENU_H

#define INTRODUCE_OPTIONS "\n----------Commands: ----------"
#define GET_USERS_CHOICE "Which one do you want to choose?: "
#define INCORRECT_INPUT "\nINCORRECT INPUT!"
#define END_RUN "back"
#define NEW_LINES "\n\n"
#define MIN_HELP_LENGTH 5
#define HELP_COMMAND_START "help "
#define MIN_SEARCH_LENGTH 7
#define SEARCH_COMMAND_START "search "
#define SAVE_OPTION "| Save menu (save file-name) |"
#define LOAD_OPTION "| Load menu from file (load file-name) |"
#define SAVE_COMMAND "save "
#define LOAD_COMMAND "load "
#define MIN_SERIALIZATION_LENGTH 5

#include <vector>
#include "MenuItem.h"

class MenuAnalyser;

class MenuSerializer;

class Menu : public MenuItem {
    friend class MenuAnalyser;

    friend class MenuSerializer;

public:
    Menu();

    Menu(std::string command, std::string name);

    Menu(std::string command, std::string name, MenuAnalyser *analyser);

    ~Menu() override;

    Menu &operator=(const Menu &copyOther);

    void Run() override;

    void addItem(MenuItem *item);

    void removeItem(std::string itemCommand);

private:
    std::vector<MenuItem *> items;

    MenuAnalyser *analyser;

    bool chooseCommandNRun(const std::string &commandName);

    bool showCommandHelp(const std::string &commandName);
};


#endif //SECOND_ASSIGNMENT_MENU_H
