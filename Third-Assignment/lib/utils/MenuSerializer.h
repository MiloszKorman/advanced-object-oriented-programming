//
// Created by Miłosz Korman on 30.10.18.
//

#ifndef THIRD_ASSIGNMENT_MENUSERIALIZER_H
#define THIRD_ASSIGNMENT_MENUSERIALIZER_H


#include <string>
#include "../console-user-interface/MenuCommand.h"
#include "MenuAnalyser.h"

class Menu;

class MenuSerializer {
public:
    static std::string serialize(Menu *start);

    static bool serializeToFile(Menu *start, std::string &fileName);

    static bool deserialize(Menu *toChange, std::string &menuTree);

    static bool deserializeFromFile(Menu *toChange, std::string &fileName);

//private:
    static std::string stringifyMenu(Menu *toSerialize);

    static std::string stringifyMenuCommand(MenuCommand *toSerialize);

    static Menu *destringifyMenu(std::string menuTree, MenuAnalyser *analyser);

    static MenuCommand *destringifyMenuCommand(std::string menuCommandTree);

    static void getNameNCommand(std::string &name, std::string &command, std::string &menuTree);
};


#endif //THIRD_ASSIGNMENT_MENUSERIALIZER_H
