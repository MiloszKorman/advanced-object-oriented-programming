//
// Created by Miłosz Korman on 06.11.18.
//

#include <iostream>
#include "MenuStringValidator.h"
#include "Utils.h"
#include "CharacterDefines.h"

bool MenuStringValidator::validate(std::string menuTree) {
    bool valid = true;
    int mistakeIndex = -1;
    int index = 0;
    char mistake = NO_MISTAKE;
    validateMenu(menuTree, valid, mistake, mistakeIndex, index);
    if (!valid) {
        std::cout << MISTAKE_STRING_INDEX << mistakeIndex << MISTAKE_STRING_CHAR << mistake << std::endl;
    }
    return valid;
}

void MenuStringValidator::validateMenu(std::string menuTree, bool &correct, char &mistake, int &mistakeIndex, int &currentIndex) {
    if (!Utils::checkSymbol(menuTree, 0, OPEN_PARENTHESES, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex;
    if (!Utils::checkSymbol(menuTree, menuTree.length() - 1, CLOSE_PARENTHESES, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex + menuTree.length() - 1;
    if (menuTree.length() > 2)
        menuTree = menuTree.substr(1, menuTree.length() - 2);
    else {
        correct = false;
        menuTree = "";
    }
    currentIndex++;
    checkNameNCommand(menuTree, correct, mistake, mistakeIndex, currentIndex);
    if (!Utils::checkSymbol(menuTree, 0, SEMICOLON, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex;
    if (menuTree.length() > 2)
        menuTree = menuTree.substr(1, menuTree.length() - 1);
    else {
        menuTree = "";
    }
    currentIndex++;
    while (!menuTree.empty() && correct) {
        if (menuTree[0] != OPEN_PARENTHESES && menuTree[0] != OPEN_BRACKET) {
            correct = false;
            mistakeIndex = currentIndex;
            int close = Utils::findClose(menuTree, OPEN_BRACKET);
            if (close == -1) {
                mistake = OPEN_PARENTHESES;
            } else {
                mistake = OPEN_BRACKET;
            }
        }
        int subMenuEnd = Utils::findClose(menuTree, menuTree[0]);
        if (subMenuEnd == -1 && correct) {
            correct = false;
            if (menuTree[0] == OPEN_PARENTHESES) {
                mistake = CLOSE_PARENTHESES;
            } else {
                mistake = CLOSE_BRACKET;
            }
            mistakeIndex = currentIndex + menuTree.length();
            int ind = findMissingClose(menuTree);
            if (ind == -1)
                mistakeIndex = currentIndex + menuTree.length();
            else
                mistakeIndex = currentIndex + ind;
        } else {
            std::string subMenu = menuTree.substr(0, subMenuEnd + 1);
            if (subMenuEnd + 1 < menuTree.length() - 1) {
                menuTree = menuTree.substr(subMenuEnd + 1, menuTree.length() - (subMenuEnd + 1));
                currentIndex += subMenuEnd + 1;
                if (!Utils::checkSymbol(menuTree, 0, COMMA, correct, mistake) && mistakeIndex == -1)
                    mistakeIndex = currentIndex;
                if (menuTree.length() > 1)
                    menuTree = menuTree.substr(1, menuTree.length() - 1);
                else {
                    correct = false;
                    menuTree = "";
                }
                currentIndex++;
            } else {
                menuTree = "";
            }
            if (correct) {
                if (subMenu[0] == OPEN_PARENTHESES) {
                    validateMenu(subMenu, correct, mistake, mistakeIndex, currentIndex);
                } else {
                    validateMenuCommand(subMenu, correct, mistake, mistakeIndex, currentIndex);
                }
            }
        }

    }
}

void MenuStringValidator::validateMenuCommand(std::string menuCommandTree, bool &correct, char &mistake, int &mistakeIndex,
                                         int &currentIndex) {
    if (!Utils::checkSymbol(menuCommandTree, 0, OPEN_BRACKET, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex;
    if (!Utils::checkSymbol(menuCommandTree, menuCommandTree.length() - 1, CLOSE_BRACKET, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex + menuCommandTree.length();
    menuCommandTree = menuCommandTree.substr(1, menuCommandTree.length() - 2);
    currentIndex++;
    checkNameNCommand(menuCommandTree, correct, mistake, mistakeIndex, currentIndex);
    if (!Utils::checkSymbol(menuCommandTree, 0, COMMA, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex;
    menuCommandTree = menuCommandTree.substr(1, menuCommandTree.length() - 1);
    currentIndex++;
    if (!Utils::checkSymbol(menuCommandTree, 0, APOSTROPHE, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex;
    if (Utils::findClose(menuCommandTree, APOSTROPHE) != menuCommandTree.length() - 1 && correct) {
        correct = false;
        if (mistakeIndex == -1) {
            mistakeIndex = currentIndex + menuCommandTree.length() - 1;
            mistake = APOSTROPHE;
        }
    }
}

void MenuStringValidator::checkNameNCommand(std::string &menuTree, bool &correct, char &mistake, int &mistakeIndex,
                                       int &currentIndex) {
    if (!Utils::checkSymbol(menuTree, 0, APOSTROPHE, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex;
    int commandEnd = Utils::findClose(menuTree, APOSTROPHE);
    if (commandEnd == -1) {
        correct = false;
        if (mistakeIndex == -1) {
            mistakeIndex = currentIndex + commandEnd;
            mistake = APOSTROPHE;
        }
    }
    if (menuTree[commandEnd - 1] == COMMA) {
        correct = false;
        mistakeIndex = commandEnd - 1;
        mistake = APOSTROPHE;
    }
    if (!Utils::checkSymbol(menuTree, commandEnd + 1, COMMA, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex + commandEnd + 1;
    if (menuTree.length() > commandEnd + 2)
        menuTree = menuTree.substr(commandEnd + 2, menuTree.length() - (commandEnd + 2));
    currentIndex += commandEnd + 2;
    if (!Utils::checkSymbol(menuTree, 0, APOSTROPHE, correct, mistake) && mistakeIndex == -1)
        mistakeIndex = currentIndex;
    int menuEnd = Utils::findClose(menuTree, APOSTROPHE);
    if (menuEnd == -1) {
        correct = false;
        if (mistakeIndex == -1) {
            mistakeIndex = currentIndex;
            mistake = APOSTROPHE;
        }
    }
    if (menuTree[menuEnd - 1] == COMMA) {
        correct = false;
        mistakeIndex = commandEnd - 1;
        mistake = APOSTROPHE;
    }
    if (menuTree.length() > menuEnd + 1)
        menuTree = menuTree.substr(menuEnd + 1, menuTree.length() - (menuEnd + 1));
    currentIndex += menuEnd + 1;
}

int MenuStringValidator::findMissingClose(std::string &menuTree) {
    int missingIndex = -1;

    int openings = 1;
    for (int i = 1; i < menuTree.size(); ++i) {
        if (menuTree[i] == OPEN_PARENTHESES || menuTree[i] == OPEN_BRACKET) {
            if (menuTree[i-1] == COMMA && openings == 1) {
                return i-2;
            } else {
                openings++;
            }
        }
    }

    return missingIndex;
}