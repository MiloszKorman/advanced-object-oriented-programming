//
// Created by Miłosz Korman on 30.10.18.
//

#include <iostream>
#include "MenuAnalyser.h"
#include "../console-user-interface/Menu.h"

MenuAnalyser::~MenuAnalyser() {
    std::cout << MENU_ANALYSER_DESTRUCTOR << std::endl;
}

void MenuAnalyser::setMainMenu(Menu *mainMenu) {
    this->mainMenu = mainMenu;
}

void MenuAnalyser::searchForCommandNPrint(Menu *menu, const std::string soughtCommand, std::string path, bool &found) {
    if (mainMenu) {
        if (!menu)
            menu = mainMenu;
        path += menu->getCommand();
        path += ARROW;
        std::vector<MenuItem *> items = menu->items;
        for (int i = 0; i < items.size(); ++i) {
            if (items[i]->getCommand() == soughtCommand){
                found = true;
                std::cout << path << soughtCommand << std::endl;
            }
            if (Menu *next = dynamic_cast<Menu *>(items[i]))
                searchForCommandNPrint(next, soughtCommand, path, found);
        }
    }
}
