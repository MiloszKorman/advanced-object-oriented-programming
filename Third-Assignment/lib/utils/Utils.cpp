//
// Created by Miłosz Korman on 21.10.18.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include "Utils.h"
#include "CharacterDefines.h"

std::string Utils::getString() {
    std::string response;
    while (response.empty())
        std::getline(std::cin, response);

    return response;
}

bool Utils::getBoolean() {
    std::string response;
    char key;
    do {
        std::cout << YES_OR_NO;
        std::cin >> response;
        key = response.at(0);

        if (key == BIG_YES || key == SMALL_YES) return true;
        if (key == BIG_NO || key == SMALL_NO) return false;

    } while (key == BIG_YES || key == SMALL_YES || key == BIG_NO || key == SMALL_NO);

    return false; //should never come here
}

int Utils::getIntFrom(int bottom) {
    std::cout << GET_INT_FROM << bottom << COLON_SPACE;

    int response = bottom - 1;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger || response < bottom);

    return response;
}

int Utils::getIntBetween(int bottom, int top) {
    std::cout << GET_AN_INTEGER_BETWEEN << bottom << SPACE_AND_SPACE << top << COLON_SPACE;

    int response = bottom - 1;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger || response < bottom || top < response);

    return response;
}

int Utils::getAnyInt() {
    std::cout << GET_AN_INTEGER;

    int response;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger);

    return response;
}

bool Utils::checkSymbol(std::string &string, int index, char symbol, bool &correct, char &mistake) {
    if (0 <= index && index < string.length() && correct) {
        if (string[index] != symbol) {
            correct = false;
            if (mistake == NO_MISTAKE)
                mistake = symbol;
            return false;
        }
        return true;
    }
    return false;
}

bool Utils::writeToFile(std::string &content, std::string &fileName) {
    std::ofstream fileStream(fileName);

    if (fileStream.is_open()) {
        fileStream << content << std::endl;
        fileStream.close();
        return true;
    } else {
        fileStream.close();
        return false;
    }
}

std::string Utils::readFromFile(std::string &fileName) {
    std::ifstream fileStream(fileName);

    std::string menuTree;

    if (fileStream.is_open()) {
        std::getline(fileStream, menuTree);
    }

    fileStream.close();
    return menuTree;
}

int Utils::findClose(std::string &menuTree, char open) {
    char close;

    switch (open) {
        case APOSTROPHE:
            for (int i = 1; i < menuTree.length(); ++i) {
                if (menuTree[i] == open)
                    return i;
            }
            return -1;
        case OPEN_PARENTHESES:
            close = CLOSE_PARENTHESES;
            break;
        case OPEN_BRACKET:
            close = CLOSE_BRACKET;
            break;
        default:
            return -1;
    }

    int occurrences = 1;

    for (int i = 1; i < menuTree.length(); ++i) {
        if (menuTree[i] == open) {
            occurrences++;
        } else if (menuTree[i] == close) {
            occurrences--;
            if (occurrences == 0)
                return i;
        }
    }

    return -1;
}
