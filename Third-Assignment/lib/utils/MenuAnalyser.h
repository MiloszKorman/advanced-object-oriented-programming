//
// Created by Miłosz Korman on 30.10.18.
//

#ifndef THIRD_ASSIGNMENT_MENUANALYSER_H
#define THIRD_ASSIGNMENT_MENUANALYSER_H


#define MENU_ANALYSER_DESTRUCTOR "### Deleting Menu Analyser ###"
#define ARROW " -> "

#include <string>

class Menu;
class MenuAnalyser {
public:
    ~MenuAnalyser();

    void setMainMenu(Menu *mainMenu);

    void searchForCommandNPrint(Menu *menu, const std::string soughtCommand, std::string path, bool &found);

private:
    Menu *mainMenu = NULL;
};


#endif //THIRD_ASSIGNMENT_MENUANALYSER_H
