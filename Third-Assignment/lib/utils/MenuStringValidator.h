//
// Created by Miłosz Korman on 06.11.18.
//

#ifndef THIRD_ASSIGNMENT_MENUSTRINGVALIDATOR_H
#define THIRD_ASSIGNMENT_MENUSTRINGVALIDATOR_H


#define NO_MISTAKE 'a'
#define MISTAKE_STRING_INDEX "On index: "
#define MISTAKE_STRING_CHAR " expected: "

#include <string>

class MenuStringValidator {
public:
    static bool validate(std::string menuTree);

private:
    static void validateMenu(std::string menuTree, bool &correct, char &mistake, int &mistakeIndex, int &currentIndex);

    static void validateMenuCommand(std::string menuCommandTree, bool &correct, char &mistake, int &mistakeIndex, int &currentIndex);

    static void checkNameNCommand(std::string &menuTree, bool &correct, char &mistake, int &mistakeIndex, int &currentIndex);

    static int findMissingClose(std::string &menuTree);
};


#endif //THIRD_ASSIGNMENT_MENUSTRINGVALIDATOR_H
