//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_UTILS_H
#define SECOND_ASSIGNMENT_UTILS_H


#include <string>

#define GET_INT_FROM "Provide an integer equal or greater than "
#define NO_MISTAKE 'a'

#define GET_AN_INTEGER "Provide an integer: "

#define GET_AN_INTEGER_BETWEEN "Provide an integer between "

#define SPACE_AND_SPACE " and "

#define COLON_SPACE ": "

#define YES_OR_NO "(Y/N)?: "

#define BIG_YES 'Y'

#define BIG_NO 'N'

#define SMALL_YES 'y'

#define SMALL_NO 'n'

class Utils {
public:
    static std::string getString();

    static bool getBoolean();

    static int getIntFrom(int bottom);

    static int getIntBetween(int bottom, int top);

    static int getAnyInt();

    static bool checkSymbol(std::string &string, int index, char symbol, bool &correct, char &mistake);

    static bool writeToFile(std::string &content, std::string &fileName);

    static std::string readFromFile(std::string &fileName);

    static int findClose(std::string &menuTree, char open);
};


#endif //SECOND_ASSIGNMENT_UTILS_H
