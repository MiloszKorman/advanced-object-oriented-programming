//
// Created by Miłosz Korman on 30.10.18.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include "MenuSerializer.h"
#include "../console-user-interface/Menu.h"
#include "Utils.h"
#include "MenuStringValidator.h"
#include "CharacterDefines.h"

std::string MenuSerializer::serialize(Menu *start) {
    return stringifyMenu(start);
}

bool MenuSerializer::serializeToFile(Menu *start, std::string &fileName) {
    std::string menuTree = serialize(start);
    return Utils::writeToFile(menuTree, fileName);
}

bool MenuSerializer::deserialize(Menu *toChange, std::string &menuTree) {
    if (MenuStringValidator::validate(menuTree)) {
        Menu *temp = destringifyMenu(menuTree, toChange->analyser);
        *toChange = *temp;
        temp->items.clear();
        delete temp;
        return true;
    } else
        return false;
}

bool MenuSerializer::deserializeFromFile(Menu *toChange, std::string &fileName) {
    std::string menuTree = Utils::readFromFile(fileName);
    return deserialize(toChange, menuTree);
}

std::string MenuSerializer::stringifyMenu(Menu *toSerialize) {
    std::stringstream menuRepresentation;
    menuRepresentation << OPEN_PARENTHESES << APOSTROPHE << toSerialize->getName() << APOSTROPHE
                       << COMMA << APOSTROPHE << toSerialize->getCommand() << APOSTROPHE << SEMICOLON;
    for (int i = 0; i < toSerialize->items.size(); ++i) {
        if (MenuCommand *menuCommand = dynamic_cast<MenuCommand *>(toSerialize->items[i]))
            menuRepresentation << stringifyMenuCommand(menuCommand);
        else
            menuRepresentation << stringifyMenu(dynamic_cast<Menu *>(toSerialize->items[i]));
        if (i != toSerialize->items.size() - 1)
            menuRepresentation << COMMA;
    }
    menuRepresentation << CLOSE_PARENTHESES;
    return menuRepresentation.str();
}

std::string MenuSerializer::stringifyMenuCommand(MenuCommand *toSerialize) {
    std::stringstream menuCommandRepresentation;
    menuCommandRepresentation << OPEN_BRACKET << APOSTROPHE << toSerialize->getName() << APOSTROPHE << COMMA
                              << APOSTROPHE << toSerialize->getCommand() << APOSTROPHE << COMMA
                              << APOSTROPHE << toSerialize->getHelp() << APOSTROPHE << CLOSE_BRACKET;
    return menuCommandRepresentation.str();
}

Menu *MenuSerializer::destringifyMenu(std::string menuTree, MenuAnalyser *analyser) {
    std::string name;
    std::string command;
    getNameNCommand(name, command, menuTree);
    Menu *menu = new Menu(command, name, analyser);
    while (!menuTree.empty()) {
        int subMenuEnd = Utils::findClose(menuTree, menuTree[0]);
        std::string subMenu = menuTree.substr(0, subMenuEnd + 1);
        if (subMenuEnd + 2 < menuTree.length() - 1) {
            menuTree = menuTree.substr(subMenuEnd + 2, menuTree.length() - (subMenuEnd + 2));
        } else {
            menuTree = "";
        }
        if (subMenu[0] == OPEN_PARENTHESES) {
            menu->addItem(destringifyMenu(subMenu, analyser));
        } else {
            menu->addItem(destringifyMenuCommand(subMenu));
        }
    }
    return menu;
}

MenuCommand *MenuSerializer::destringifyMenuCommand(std::string menuCommandTree) {
    MenuCommand *menuCommand;
    std::string name;
    std::string command;
    getNameNCommand(name, command, menuCommandTree);
    std::string help = menuCommandTree.substr(1, menuCommandTree.length() - 2);
    menuCommand = new MenuCommand(command, name, NULL, help);
    return menuCommand;
}

void MenuSerializer::getNameNCommand(std::string &name, std::string &command, std::string &menuTree) {
    menuTree = menuTree.substr(1, menuTree.length() - 2);
    int nameEnd = Utils::findClose(menuTree, APOSTROPHE);
    name = menuTree.substr(1, nameEnd - 1);
    menuTree = menuTree.substr(nameEnd + 2, menuTree.length() - (nameEnd + 2));
    int commandEnd = Utils::findClose(menuTree, APOSTROPHE);
    command = menuTree.substr(1, commandEnd - 1);
    menuTree = menuTree.substr(commandEnd + 2, menuTree.length() - (commandEnd + 2));
}
