//
// Created by Miłosz Korman on 22.10.18.
//

#include "TableHandler.h"

TableHandler::~TableHandler() {
    std::cout << DESTRUCTOR_MESSAGE << std::endl;
}

std::vector<Table *> &TableHandler::getTables() {
    return tables;
}
