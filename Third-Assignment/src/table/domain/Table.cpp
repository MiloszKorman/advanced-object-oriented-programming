//
// Created by Miłosz Korman on 06.10.18.
//

#include "Table.h"

/*
 * Constructors
 */
Table::Table() {
    name = DEFAULT_NAME;
    tableLength = DEFAULT_LENGTH;
    array = new int[tableLength];
    populateWithZeros(0, tableLength);

    std::cout << CREATING_DEFAULT << name << TRIPLE_HASH << std::endl;
}

Table::Table(std::string name, int tableLength) {
    this->name = name;
    this->tableLength = tableLength;
    array = new int[tableLength];
    populateWithZeros(0, tableLength);

    std::cout << CREATING_PARAMETERED << name << TRIPLE_HASH << std::endl;
}

Table::Table(Table &pcOther) {
    name = pcOther.name + COPY;
    tableLength = pcOther.tableLength;
    array = new int[tableLength];

    for (int i = 0; i < tableLength; i++)
        *(array + i) = *(pcOther.array + i);

    std::cout << CREATING_COPIED << name << TRIPLE_HASH << std::endl;
}


/*
 * Destructor
 */
Table::~Table() {
    delete[] array;
    std::cout << MENU_ITEM_DESTRUCTOR << name << TRIPLE_HASH << std::endl;
}


/*
 * Public Methods
 */
std::string Table::getName() {
    return name;
}

void Table::setName(std::string newName) {
    this->name = std::move(newName);
}

int Table::getLength() {
    return tableLength;
}

bool Table::setLength(int newLength) {
    if (0 <= newLength) {
        int *newArray;
        newArray = new int[newLength];

        int sizeToCopy = (newLength < tableLength) ? newLength : tableLength;
        std::memcpy(newArray, array, sizeof(int) * sizeToCopy); //copy all elements to new array

        delete[] array; //free space from old array
        array = newArray; //assign new array as main array

        populateWithZeros(tableLength, newLength);
        tableLength = newLength; //assign new length as tables length

        return true;
    } else return false;
}

//Returns -1 when illegal index is tried to be accessed
int Table::getElement(int index, bool *success) {
    if (checkRange(index, 0, tableLength)) {
        *success = true;
        return array[index];
    } else {
        *success = false;
        return -1;
    }
}

bool Table::setElement(int index, int value) {
    if (checkRange(index, 0, tableLength)) {
        array[index] = value;
        return true;
    } else return false;

}

Table *Table::clone() {
    return new Table(*this);
}

Table &Table::operator=(Table &pcOther) {
    tableLength = pcOther.tableLength;
    int *newArray = new int[tableLength];

    for (int i = 0; i < tableLength; i++)
        *(newArray + i) = *(pcOther.array + i);

    delete[] array;
    array = newArray;

    return *this;
}

std::string Table::toString() {
    std::stringstream objectRepresentation;
    objectRepresentation << name;
    objectRepresentation << LENGTH << tableLength;
    objectRepresentation << VALUES;

    for (int i = 0; i < tableLength; i++) {
        objectRepresentation << array[i];

        if (i != tableLength - 1) {
            objectRepresentation << ", ";
        }
    }

    return objectRepresentation.str();
}

void Table::doubleArr() {
    int *newTable = new int[tableLength * DOUBLE];

    for (int i = 0; i < tableLength; i++) {
        *(newTable + i) = *(array + i);
        *(newTable + tableLength + i) = *(array + i);
    }

    delete[] array;
    array = newTable;
    tableLength *= DOUBLE;
}

void Table::populateWithZeros(int start, int finish) {
    if (0 <= start && start < finish)
        for (int i = start; i < finish; i++)
            *(array + i) = 0;
}

bool Table::checkRange(int index, int start, int finish) {
    return (start <= index && index < finish);
}