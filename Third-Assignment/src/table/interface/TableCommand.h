//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLECOMMAND_H
#define SECOND_ASSIGNMENT_TABLECOMMAND_H

#define EMPTY_LIST "\nCan't operate on empty list!"
#define GET_INDEX "\nSupply index"

#include "../../../lib/console-user-interface/Command.h"
#include "../domain/TableHandler.h"

class TableCommand : public Command {
public:
    explicit TableCommand(TableHandler &handler);

protected:
    TableHandler* handler;
};


#endif //SECOND_ASSIGNMENT_TABLECOMMAND_H
