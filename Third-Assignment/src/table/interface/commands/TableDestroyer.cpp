//
// Created by Miłosz Korman on 23.10.18.
//

#include "TableDestroyer.h"
#include "../../../../lib/utils/Utils.h"

TableDestroyer::TableDestroyer(TableHandler &handler) : TableCommand(handler) {
}

void TableDestroyer::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    if (tables->empty())
        std::cout << EMPTY_LIST << std::endl;
    else {
        std::cout << GET_INDEX << std::endl;
        int index = Utils::getIntBetween(0, tables->size() - 1);
        delete (*tables)[index];
        tables->erase(tables->begin() + index);
    }
}
