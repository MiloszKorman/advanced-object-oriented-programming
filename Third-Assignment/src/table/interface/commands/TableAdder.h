//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLEADDITIONCOMMAND_H
#define SECOND_ASSIGNMENT_TABLEADDITIONCOMMAND_H

#define SUPPLY_PARAMETERS "Do you want to supply parameters?: "
#define GET_TABLE_NAME "What do you want to name it?: "
#define GET_TABLE_LENGTH "How many fields should it have?: "


#include "../TableCommand.h"

class TableAdder : public TableCommand {
public:
    explicit TableAdder(TableHandler &handler);

    void RunCommand() override;

protected:
    void addDefaultTable();

    void addParameterizedTable();
};


#endif //SECOND_ASSIGNMENT_TABLEADDITIONCOMMAND_H
