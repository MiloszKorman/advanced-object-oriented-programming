//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLENAMECHANGER_H
#define SECOND_ASSIGNMENT_TABLENAMECHANGER_H


#define GET_NEW_NAME "What the new name should be?: "

#include "../TableCommand.h"

class TableNameChanger : public TableCommand {
public:
    explicit TableNameChanger(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_TABLENAMECHANGER_H
