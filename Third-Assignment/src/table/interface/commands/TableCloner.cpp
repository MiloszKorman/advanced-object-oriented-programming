//
// Created by Miłosz Korman on 23.10.18.
//

#include "TableCloner.h"
#include "../../../../lib/utils/Utils.h"

TableCloner::TableCloner(TableHandler &handler) : TableCommand(handler) {
}

void TableCloner::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    if (tables->empty())
        std::cout << EMPTY_LIST << std::endl;
    else {
        std::cout << GET_INDEX << std::endl;
        int index = Utils::getIntBetween(0, tables->size() - 1);
        tables->push_back((*tables)[index]->clone());
    }
}
