//
// Created by Miłosz Korman on 23.10.18.
//

#include "TableNameChanger.h"
#include "../../../../lib/utils/Utils.h"

TableNameChanger::TableNameChanger(TableHandler &handler) : TableCommand(handler) {
}

void TableNameChanger::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    if (tables->empty())
        std::cout << EMPTY_LIST << std::endl;
    else {
        std::cout << GET_INDEX << std::endl;
        int index = Utils::getIntBetween(0, tables->size() - 1);
        std::cout << GET_NEW_NAME;
        std::string newName = Utils::getString();
        (*tables)[index]->setName(newName);
    }
}
