//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_ALLTABLEPRINTER_H
#define SECOND_ASSIGNMENT_ALLTABLEPRINTER_H


#include "TablePrinter.h"

class AllTablePrinter : public TablePrinter {
public:
    explicit AllTablePrinter(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_ALLTABLEPRINTER_H
