//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLESIZECHANGER_H
#define SECOND_ASSIGNMENT_TABLESIZECHANGER_H

#define GET_NEW_LENGTH "What the new length should be?"


#include "../TableCommand.h"

class TableSizeChanger : public TableCommand {
public:
    explicit TableSizeChanger(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_TABLESIZECHANGER_H
