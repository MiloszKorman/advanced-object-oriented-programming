//
// Created by Miłosz Korman on 23.10.18.
//

#include "TableFieldChanger.h"
#include "../../../../lib/utils/Utils.h"

TableFieldChanger::TableFieldChanger(TableHandler &handler) : TableCommand(handler) {
}

void TableFieldChanger::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    if (tables->empty())
        std::cout << EMPTY_LIST << std::endl;
    else {
        std::cout << GET_INDEX << std::endl;
        int index = Utils::getIntBetween(0, tables->size() - 1);
        std::cout << GET_PLACE_TO_CHANGE << std::endl;
        int valueIndex = Utils::getIntBetween(0, (*tables)[index]->getLength() - 1);
        std::cout << GET_NEW_VALUE << std::endl;
        int newValue = Utils::getAnyInt();
        (*tables)[index]->setElement(valueIndex, newValue);
    }
}
