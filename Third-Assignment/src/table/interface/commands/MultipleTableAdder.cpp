//
// Created by Miłosz Korman on 23.10.18.
//

#include "MultipleTableAdder.h"
#include "../../../../lib/utils/Utils.h"

MultipleTableAdder::MultipleTableAdder(TableHandler &handler) : TableAdder(handler) {
}

void MultipleTableAdder::RunCommand() {
    std::cout << SUPPLY_PARAMETERS;
    bool parameterized = Utils::getBoolean();
    std::cout << GET_AMOUNT << std::endl;
    int amount = Utils::getIntFrom(1);
    for (int i = 0; i < amount; ++i)
        if (parameterized)
            addParameterizedTable();
        else
            addDefaultTable();

}
