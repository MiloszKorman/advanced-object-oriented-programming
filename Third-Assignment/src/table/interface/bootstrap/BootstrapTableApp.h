//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_BOOTSTRAPTABLEAPP_H
#define SECOND_ASSIGNMENT_BOOTSTRAPTABLEAPP_H

#define MAIN_MENU_COMMAND "menu"
#define ADDING_MENU_COMMAND "add"
#define ADD_ONE_COMMAND "add one"
#define ADD_MULTIPLE_COMMAND "add multi"
#define PRINTING_MENU_COMMAND "print"
#define PRINT_ONE_COMMAND "print one"
#define PRINT_ALL_COMMAND "print all"
#define MODIFYING_MENU_COMMAND "modify"
#define CHANGE_NAME_COMMAND "change name"
#define CHANGE_SIZE_COMMAND "change size"
#define CHANGE_VALUE_COMMAND "change value"
#define CLONING_MENU_COMMAND "clone"
#define CLONE_COMMAND "clone"
#define DESTROYING_MENU_COMMAND "destroy"
#define DESTROY_ONE_COMMAND "destroy one"
#define DESTROY_ALL_COMMAND "destroy all"
#define MAIN_MENU_NAME "Main Menu"
#define ADDING_MENU_NAME "Adding Menu"
#define ADD_ONE_NAME "Add one Table"
#define ADD_MULTIPLE_NAME "Add multiple Tables"
#define PRINTING_MENU_NAME "Printing Menu"
#define PRINT_ONE_NAME "Print one Table"
#define PRINT_ALL_NAME "Print all Tables"
#define MODIFYING_MENU_NAME "Modifying Menu"
#define CHANGE_NAME_NAME "Change Table name"
#define CHANGE_SIZE_NAME "Change Table size"
#define CHANGE_VALUE_NAME "Change value in Table field"
#define CLONING_MENU_NAME "Cloning Menu"
#define CLONE_NAME "Clone and add Table"
#define DESTROYING_MENU_NAME "Destroying Menu"
#define DESTROY_ONE_NAME "Destroy one Table"
#define DESTROY_ALL_NAME "Destroy all Tables"


#define ADD_ONE_HELPER_TEXT "Adds one Table to TableHandler"

#include "../../../../lib/console-user-interface/Menu.h"
#include "../../domain/TableHandler.h"

class BootstrapTableApp {
public:
    Menu *getMainMenu(TableHandler &tableHandler, MenuAnalyser *menuAnalyser);
};


#endif //SECOND_ASSIGNMENT_BOOTSTRAPTABLEAPP_H
