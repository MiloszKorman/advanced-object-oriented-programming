//
// Created by Miłosz Korman on 23.10.18.
//

#include "BootstrapTableApp.h"
#include "../../../../lib/console-user-interface/MenuCommand.h"
#include "../commands/TableAdder.h"
#include "../commands/MultipleTableAdder.h"
#include "../commands/TablePrinter.h"
#include "../commands/AllTablePrinter.h"
#include "../commands/TableNameChanger.h"
#include "../commands/TableSizeChanger.h"
#include "../commands/TableFieldChanger.h"
#include "../commands/TableCloner.h"
#include "../commands/TableDestroyer.h"
#include "../commands/AllTableDestroyer.h"
#include "../../../../lib/utils/MenuAnalyser.h"

Menu *BootstrapTableApp::getMainMenu(TableHandler &tableHandler, MenuAnalyser *menuAnalyser) {
    Menu *menu = new Menu(MAIN_MENU_COMMAND, MAIN_MENU_NAME, menuAnalyser);

    Menu *addingMenu = new Menu(ADDING_MENU_COMMAND, ADDING_MENU_NAME, menuAnalyser);
    addingMenu->addItem(new MenuCommand(ADD_ONE_COMMAND, ADD_ONE_NAME, new TableAdder(tableHandler), ADD_ONE_HELPER_TEXT));
    addingMenu->addItem(new MenuCommand(ADD_MULTIPLE_COMMAND, ADD_MULTIPLE_NAME, new MultipleTableAdder(tableHandler)));
    menu->addItem(addingMenu);

    Menu *printingMenu = new Menu(PRINTING_MENU_COMMAND, PRINTING_MENU_NAME, menuAnalyser);
    printingMenu->addItem(new MenuCommand(PRINT_ONE_COMMAND, PRINT_ONE_NAME, new TablePrinter(tableHandler)));
    printingMenu->addItem(new MenuCommand(PRINT_ALL_COMMAND, PRINT_ALL_NAME, new AllTablePrinter(tableHandler)));
    menu->addItem(printingMenu);

    Menu *modificatingMenu = new Menu(MODIFYING_MENU_COMMAND, MODIFYING_MENU_NAME, menuAnalyser);
    modificatingMenu->addItem(
            new MenuCommand(CHANGE_NAME_COMMAND, CHANGE_NAME_NAME, new TableNameChanger(tableHandler)));
    modificatingMenu->addItem(
            new MenuCommand(CHANGE_SIZE_COMMAND, CHANGE_SIZE_NAME, new TableSizeChanger(tableHandler)));
    modificatingMenu->addItem(
            new MenuCommand(CHANGE_VALUE_COMMAND, CHANGE_VALUE_NAME, new TableFieldChanger(tableHandler)));
    menu->addItem(modificatingMenu);

    Menu *cloningMenu = new Menu(CLONING_MENU_COMMAND, CLONING_MENU_NAME, menuAnalyser);
    cloningMenu->addItem(new MenuCommand(CLONE_COMMAND, CLONE_NAME, new TableCloner(tableHandler)));
    menu->addItem(cloningMenu);

    Menu *destroyingMenu = new Menu(DESTROYING_MENU_COMMAND, DESTROYING_MENU_NAME, menuAnalyser);
    destroyingMenu->addItem(new MenuCommand(DESTROY_ONE_COMMAND, DESTROY_ONE_NAME, new TableDestroyer(tableHandler)));
    destroyingMenu->addItem(
            new MenuCommand(DESTROY_ALL_COMMAND, DESTROY_ALL_NAME, new AllTableDestroyer(tableHandler)));
    menu->addItem(destroyingMenu);

    menuAnalyser->setMainMenu(menu);

    return menu;
}
