#include <iostream>
#include "table/interface/bootstrap/BootstrapTableApp.h"

#include "../lib/utils/MenuAnalyser.h"
#include "../lib/utils/MenuSerializer.h"
#include "../lib/utils/MenuStringValidator.h"

int main() {
    BootstrapTableApp bootstrap;

    TableHandler *tableHandler = new TableHandler;
    MenuAnalyser *menuAnalyser = new MenuAnalyser;

    Menu *mainMenu = bootstrap.getMainMenu(*tableHandler, menuAnalyser);

    mainMenu->Run();

    delete mainMenu;
    delete menuAnalyser;
    delete tableHandler;

    /*std::string filename = "menu.txt";
    MenuAnalyser analyser;
    Menu *menu = new Menu("default", "Default", &analyser);
    analyser.setMainMenu(menu);
    MenuSerializer::deserializeFromFile(menu, filename);
    menu->Run();
    delete menu;*/

    /*std::string wrongString = "('Boniek','bonius';['Command','command','Default help text'],('Sub Menu','sub';('Marciniak','andrzej';),['SubMenu Commmand','sub command','Default help text'],['SubMenu Command 2','sub command2','Default help text']))";
    MenuStringValidator::validate(wrongString);*/

    return 0;
}
