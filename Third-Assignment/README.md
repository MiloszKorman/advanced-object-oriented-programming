```
Wrocław 2018.09.
Autor: Michał Przewoźniczek
```
```
Zaawansowane Metody Programowania Obiektowego – zadanie 3
Użycie obiektów, klasy wirtualne, metody wirtualne, przetwarzanie drzew, obsługa
błędów, wczytywanie z pliku
```
## UWAGA:

1. Pisząc własny program można użyć innego nazewnictwa niż to przedstawione w
    treści zadania. Należy jednak użyć spójnej konwencji kodowania, zgodnie z
    wymaganiami kursu.
2. Zadanie jest rozszerzeniem poprzedniego zadania. Żeby zaliczyć zadanie nr 3, należy
    posiadać program o pełnej funkcjonalności wymaganej w ramach zadania nr 2.

Należy rozwinąć program z zadania nr 2 o następujące funkcjonalności:

1. Po wpisaniu „help <nazwa komendy>”, ma wyświetlić się opis komendy dla obiektu
    CMenuCommand z aktualnego menu, o ile komenda o takiej nazwie istnieje. Jeżeli
    komenda nie istnieje, należy wyprowadzić komunikat „brak komendy”.

Na przykład:
Jeśli dla menu głównego z przykładu do zadania nr 2 wpiszemy „help ala ” to wyświetli się opis
dla komendy ala

2. Po wpisaniu „search <nazwa komendy>”, program ma przeszukać całe drzewo po kątem
    istnienia komend o zadanej nazwie. Należy wypisać wszystkie komendy wraz ze
    ścieżkami, w których występują.

Na przykład:
Jeśli dla drzewa menu z przykładu do zadania nr 2 wpiszemy następujące komendy:
menu
search ala
to pomimo, że komenda menu1 spowodowała wejście w podmenu, komenda search i tak znajdzie
i wypisze ścieżkę do komendy ala :
menu->ala

3. Należy wykonać zapis aktualnego drzewa menu do zmiennej typu string i ze zmiennej
    typu string. Zapis ma mieć następujący format. Uwaga: format nie musi uwzględniać
    konkretnych funkcji przechowywanych przez obiekty dziedziczące po klasie wirtualnej
    CCommand.

Format zapisu dla menu:
(‘<nazwa menu>’,‘<polecenie otwierające menu>’;<dziecko1>,<dziecko2>,...,<dziecko n>)

Format zapisu dla komend:
[‘<nazwa komendy>’,‘< polecenie wywołujące komendę>’,‘<treść help dla komendy>’]


4. W przypadku gdyby string, z którego następuje wczytywanie, był błędny program
    powinien poinformować o tym, gdzie w stringu wystąpił błąd i jakiego znaku program
    oczekiwał.

Przykład zapisu drzewa menu z poprzedniego zadania:
(‘menu główne’,‘main’;(’Podmenu1’,’menu1’;[‘Uruchom przeglądarkę’,’internet’,’otwiera
przeglądarkę’],(’Podmenu1’,’menu1’;)),(’Podmenu2’,’menu2’;[‘Test’,’test’,’pomoc dla test’],
[‘Default command’,’defcom’,’pomoc dla test’]),[‘Napisz „Ala ma kota”’,’ala’,’napis o Ali’])

Przykład wczytywania drzewa z poniższego stringa, znajduje się na przedstawionych poniżej
rysunkach.
(‘mMenu’,‘main’;(’Subm’,’sub’;[‘MyComm’,’Comm’,’helpCom’]),(’Subm2’,’sub2’;))
