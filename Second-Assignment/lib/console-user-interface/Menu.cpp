//
// Created by Miłosz Korman on 21.10.18.
//

#include <iostream>
#include <algorithm>
#include "Menu.h"
#include "../utils/Utils.h"

Menu::Menu() : MenuItem() {
}

Menu::Menu(const std::string &command, const std::string &name) : MenuItem(command, name) {
}

Menu::~Menu() {
    for (int i = 0; i < items.size(); ++i)
        delete items[i];
    items.clear();
}

void Menu::Run() {

    std::string userChoice;
    bool correct;

    do {
        std::cout << NEW_LINES << this->toString() << std::endl;

        std::cout << INTRODUCE_OPTIONS << std::endl;
        for (int i = 0; i < items.size(); ++i)
            std::cout << items[i]->toString() << std::endl;

        std::cout << GET_USERS_CHOICE;

        userChoice = Utils::getString();
        transform(userChoice.begin(), userChoice.end(), userChoice.begin(), ::tolower);
        correct = chooseCommand(userChoice);

        if (!correct && userChoice != END_RUN)
            std::cout << INCORRECT_INPUT << std::endl;

    } while (userChoice != END_RUN);
}

void Menu::addItem(MenuItem *item) {
    bool unique = true;
    for (int i = 0; i < items.size(); ++i)
        if (item->getName() == items[i]->getName() || item->getCommand() == items[i]->getCommand())
            unique = false;

    if (unique)
        items.push_back(item);
}

void Menu::removeItem(std::string itemName) {
    for (int i = 0; i < items.size(); ++i)
        if (itemName == items[i]->getCommand())
            items.erase(items.begin() + i);
}

bool Menu::chooseCommand(const std::string &commandName) {
    for (int i = 0; i < items.size(); ++i)
        if (commandName == items[i]->getCommand()) {
            items[i]->Run();
            return true;
        }

    return false;
}
