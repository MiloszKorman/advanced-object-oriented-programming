//
// Created by Miłosz Korman on 21.10.18.
//

#include <iostream>
#include "MenuCommand.h"

MenuCommand::MenuCommand() : MenuItem() {
}

MenuCommand::MenuCommand(const std::string &command, const std::string &name) : MenuItem(command, name) {
}

MenuCommand::MenuCommand(const std::string &command, const std::string &name, Command *givenCommand)
        : MenuItem(command, name) {
    changeCommand(givenCommand);
}

MenuCommand::~MenuCommand() {
    if (command)
        delete command;
}

void MenuCommand::Run() {
    if (command)
        command->RunCommand();
    else
        std::cout << NULL_COMMAND << std::endl;
}

void MenuCommand::changeCommand(Command *givenCommand) {
    this->command = givenCommand;
}
