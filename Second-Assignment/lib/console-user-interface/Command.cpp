//
// Created by Miłosz Korman on 21.10.18.
//

#include <iostream>
#include "Command.h"

void Command::RunCommand() {
    std::cout << DEFAULT_COMMAND << std::endl;
}

