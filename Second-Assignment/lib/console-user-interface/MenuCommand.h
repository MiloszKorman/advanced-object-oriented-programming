//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_MENUCOMMAND_H
#define SECOND_ASSIGNMENT_MENUCOMMAND_H


#define NULL_COMMAND "Empty command"

#include "MenuItem.h"
#include "Command.h"

class MenuCommand : public MenuItem {
public:
    MenuCommand();

    MenuCommand(const std::string &command, const std::string &name);

    MenuCommand(const std::string &command, const std::string &name, Command *givenCommand);

    ~MenuCommand();

    void Run() override;

    void changeCommand(Command *givenCommand);

private:
    Command *command;
};


#endif //SECOND_ASSIGNMENT_MENUCOMMAND_H
