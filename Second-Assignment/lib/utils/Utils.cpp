//
// Created by Miłosz Korman on 21.10.18.
//

#include <iostream>
#include <sstream>
#include "Utils.h"

std::string Utils::getString() {
    std::string response;
    while (response.empty())
        std::getline(std::cin, response);

    return response;
}

bool Utils::getBoolean() {
    std::string response;
    char key;
    do {
        std::cout << "(Y/N)?: ";
        std::cin >> response;
        key = response.at(0);

        if (key == 'Y' || key == 'y') return true;
        if (key == 'N' || key == 'n') return false;

    } while (key == 'Y' || key == 'y' || key == 'N' || key == 'n');

    return false; //should never come here
}

int Utils::getIntFrom(int bottom) {
    std::cout << GET_INT_FROM << bottom << ": ";

    int response = bottom - 1;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger || response < bottom);

    return response;
}

int Utils::getIntBetween(int bottom, int top) {
    std::cout << "Provide an integer between " << bottom << " and " << top << ": ";

    int response = bottom - 1;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger || response < bottom || top < response);

    return response;
}

int Utils::getAnyInt() {
    std::cout << "Provide an integer: ";

    int response;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger);

    return response;
}
