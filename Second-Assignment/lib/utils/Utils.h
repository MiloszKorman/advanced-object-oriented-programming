//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_UTILS_H
#define SECOND_ASSIGNMENT_UTILS_H


#define GET_INT_FROM "Provide an integer equal or greater than "

#include "../console-user-interface/MenuItem.h"

class Utils {
public:
    static std::string getString();

    static bool getBoolean();

    static int getIntFrom(int bottom);

    static int getIntBetween(int bottom, int top);

    static int getAnyInt();
};


#endif //SECOND_ASSIGNMENT_UTILS_H
