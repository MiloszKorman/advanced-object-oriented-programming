//
// Created by Miłosz Korman on 25.10.18.
//

#include <iostream>
#include "Addition.h"


Addition::Addition(int *a, int *b) : Calculator(a, b) {
}

void Addition::RunCommand() {
    std::cout << ADDITION_INFO << *a << PLUS_SIGN << *b << EQUAL_SIGN << *a+*b << std::endl;
}
