//
// Created by Miłosz Korman on 25.10.18.
//

#include <iostream>
#include "ChangeNum.h"
#include "../../../lib/utils/Utils.h"

ChangeNum::ChangeNum(int *num) {
    this->num = num;
}

void ChangeNum::RunCommand() {
    std::cout << NEW_VALUE_QUESTION << std::endl;
    *num = Utils::getAnyInt();
}
