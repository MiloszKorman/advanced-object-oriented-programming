//
// Created by Miłosz Korman on 25.10.18.
//

#ifndef SECOND_ASSIGNMENT_CALCULATORCOMMAND_H
#define SECOND_ASSIGNMENT_CALCULATORCOMMAND_H


#include "../../../lib/console-user-interface/Command.h"

#define EQUAL_SIGN " = "

class Calculator : public Command {
public:
    Calculator(int *a, int *b);

protected:
    int *a;
    int *b;
};


#endif //SECOND_ASSIGNMENT_CALCULATORCOMMAND_H
