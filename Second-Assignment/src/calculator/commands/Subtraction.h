//
// Created by Miłosz Korman on 25.10.18.
//

#ifndef SECOND_ASSIGNMENT_SUBTRACTIONCOMMAND_H
#define SECOND_ASSIGNMENT_SUBTRACTIONCOMMAND_H


#define SUBTRACTION_INFO "Subtraction: "

#define MINUS_SIGN " - "

#include "Calculator.h"

class Subtraction : public Calculator {
public:
    Subtraction(int *a, int *b);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_SUBTRACTIONCOMMAND_H
