//
// Created by Miłosz Korman on 25.10.18.
//

#include <iostream>
#include "Subtraction.h"

Subtraction::Subtraction(int *a, int *b) : Calculator(a, b) {
}

void Subtraction::RunCommand() {
    std::cout << SUBTRACTION_INFO << *a << MINUS_SIGN << *b << EQUAL_SIGN << *a-*b << std::endl;
}
