//
// Created by Miłosz Korman on 25.10.18.
//

#ifndef SECOND_ASSIGNMENT_RESET_H
#define SECOND_ASSIGNMENT_RESET_H


#define RESET_INFO "A and B are zeroed!"

#include "Calculator.h"

class Reset : public Calculator {
public:
    void RunCommand() override;

    Reset(int *a, int *b);
};


#endif //SECOND_ASSIGNMENT_RESET_H
