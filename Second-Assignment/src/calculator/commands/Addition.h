//
// Created by Miłosz Korman on 25.10.18.
//

#ifndef SECOND_ASSIGNMENT_ADDITIONCOMMAND_H
#define SECOND_ASSIGNMENT_ADDITIONCOMMAND_H


#define ADDITION_INFO "Addition: "
#define PLUS_SIGN " + "

#include "Calculator.h"

class Addition : public Calculator{
public:
    Addition(int *a, int *b);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_ADDITIONCOMMAND_H
