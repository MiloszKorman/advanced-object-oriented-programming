//
// Created by Miłosz Korman on 25.10.18.
//

#include <iostream>
#include "Reset.h"

Reset::Reset(int *a, int *b) : Calculator(a, b) {
}

void Reset::RunCommand() {
    *a = 0;
    *b = 0;
    std::cout << RESET_INFO << std::endl;
}
