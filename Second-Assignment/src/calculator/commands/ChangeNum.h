//
// Created by Miłosz Korman on 25.10.18.
//

#ifndef SECOND_ASSIGNMENT_CHANGENUM_H
#define SECOND_ASSIGNMENT_CHANGENUM_H


#define NEW_VALUE_QUESTION "What do you want to change it for? "

#include "../../../lib/console-user-interface/Command.h"

class ChangeNum : public Command {
public:
    ChangeNum(int *num);
    void RunCommand() override;

protected:
    int *num;
};


#endif //SECOND_ASSIGNMENT_CHANGENUM_H
