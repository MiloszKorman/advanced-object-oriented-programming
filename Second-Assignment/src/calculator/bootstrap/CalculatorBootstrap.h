//
// Created by Miłosz Korman on 25.10.18.
//

#ifndef SECOND_ASSIGNMENT_CALCULATORBOOTSTRAP_H
#define SECOND_ASSIGNMENT_CALCULATORBOOTSTRAP_H


#define MAIN_MENU_COMMAND "menu"

#define MAIN_MENU_NAME "Main menu"

#define CHANGE_A_COMMAND "change a"

#define CHANGE_B_COMMAND "change b"

#define OPERATIONS_COMMAND "op"

#define ADDITION_COMMAND "add"

#define SUBTRACTION_COMMAND "sub"

#define DEFAULT_COMMAND "default"

#define RESET_COMMAND "res"

#define CHANGE_A_NAME "Change number A"

#define CHANGE_B_NAME "Change number B"

#define OPPERATIONS_NAME "Operations"

#define RESET_NAME "Reset"

#define DEFAULT_COMMAND_COMMAND "default"

#define DEFAULT_COMMAND_NAME "Default command"

#define SUBTRACTION_NAME "Subtraction of B from A"

#define ADDITION_NAME "Addition of A and B"

#include "../../../lib/console-user-interface/Menu.h"

class CalculatorBootstrap {
public:
    Menu *getMainMenu(int *a, int *b);
};


#endif //SECOND_ASSIGNMENT_CALCULATORBOOTSTRAP_H
