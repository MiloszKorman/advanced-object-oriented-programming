//
// Created by Miłosz Korman on 25.10.18.
//

#include "CalculatorBootstrap.h"
#include "../../../lib/console-user-interface/MenuCommand.h"
#include "../commands/ChangeNum.h"
#include "../commands/Addition.h"
#include "../commands/Subtraction.h"
#include "../commands/Reset.h"

Menu *CalculatorBootstrap::getMainMenu(int *a, int *b) {

    Menu* mainMenu = new Menu(MAIN_MENU_COMMAND, MAIN_MENU_NAME);
    mainMenu->addItem(new MenuCommand(CHANGE_A_COMMAND, CHANGE_A_NAME, new ChangeNum(a)));
    mainMenu->addItem(new MenuCommand(CHANGE_B_COMMAND, CHANGE_B_NAME, new ChangeNum(b)));

    Menu *operationMenu = new Menu(OPERATIONS_COMMAND, OPPERATIONS_NAME);
    operationMenu->addItem(new MenuCommand(ADDITION_COMMAND, ADDITION_NAME, new Addition(a, b)));
    operationMenu->addItem(new MenuCommand(SUBTRACTION_COMMAND, SUBTRACTION_NAME, new Subtraction(a, b)));
    operationMenu->addItem(new MenuCommand(DEFAULT_COMMAND_COMMAND, DEFAULT_COMMAND_NAME, new Command));
    mainMenu->addItem(operationMenu);

    mainMenu->addItem(new MenuCommand(RESET_COMMAND, RESET_NAME, new Reset(a, b)));

    return mainMenu;
}
