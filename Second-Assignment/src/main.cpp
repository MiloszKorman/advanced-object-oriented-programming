#include <iostream>
#include "table/interface/bootstrap/BootstrapTableApp.h"
#include "../lib/console-user-interface/Menu.h"
#include "calculator/bootstrap/CalculatorBootstrap.h"

int main() {
    /*BootstrapTableApp bootstrap;

    TableHandler *tableHandler = new TableHandler;

    Menu *mainMenu = bootstrap.getMainMenu(*tableHandler);

    mainMenu->Run();

    delete mainMenu;
    delete tableHandler;*/

    int *a = new int();
    int *b = new int();

    CalculatorBootstrap calculatorBootstrap;
    Menu* mainMenu = calculatorBootstrap.getMainMenu(a, b);
    mainMenu->Run();

    delete mainMenu;
    delete a;
    delete b;

    return 0;
}