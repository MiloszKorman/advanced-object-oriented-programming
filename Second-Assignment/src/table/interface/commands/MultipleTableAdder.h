//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_MULTIPLETABLEADDITIONCOMMAND_H
#define SECOND_ASSIGNMENT_MULTIPLETABLEADDITIONCOMMAND_H

#define GET_AMOUNT "How many?"


#include "TableAdder.h"

class MultipleTableAdder : public TableAdder {
public:
    explicit MultipleTableAdder(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_MULTIPLETABLEADDITIONCOMMAND_H
