//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_MULTIPLETABLEDESTROYER_H
#define SECOND_ASSIGNMENT_MULTIPLETABLEDESTROYER_H


#include "TableDestroyer.h"

class AllTableDestroyer : public TableDestroyer {
public:
    explicit AllTableDestroyer(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_MULTIPLETABLEDESTROYER_H
