//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_CLONETABLECOMMAND_H
#define SECOND_ASSIGNMENT_CLONETABLECOMMAND_H


#include "../TableCommand.h"

class TableCloner : public TableCommand {
public:
    explicit TableCloner(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_CLONETABLECOMMAND_H
