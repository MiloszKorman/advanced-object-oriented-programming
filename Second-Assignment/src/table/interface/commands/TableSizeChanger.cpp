//
// Created by Miłosz Korman on 23.10.18.
//

#include "TableSizeChanger.h"
#include "../../../../lib/utils/Utils.h"

TableSizeChanger::TableSizeChanger(TableHandler &handler) : TableCommand(handler) {
}

void TableSizeChanger::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    if (tables->empty())
        std::cout << EMPTY_LIST << std::endl;
    else {
        std::cout << GET_INDEX << std::endl;
        int index = Utils::getIntBetween(0, tables->size() - 1);
        std::cout << GET_NEW_LENGTH << std::endl;
        int newLength = Utils::getIntFrom(1);
        (*tables)[index]->setLength(newLength);
    }
}
