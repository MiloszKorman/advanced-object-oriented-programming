//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLEFIELDCHANGER_H
#define SECOND_ASSIGNMENT_TABLEFIELDCHANGER_H

#define GET_PLACE_TO_CHANGE "At which place?"
#define GET_NEW_VALUE "What the new value should be?"


#include "../TableCommand.h"

class TableFieldChanger : public TableCommand {
public:
    explicit TableFieldChanger(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_TABLEFIELDCHANGER_H
