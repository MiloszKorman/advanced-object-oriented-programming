//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLEPRINTER_H
#define SECOND_ASSIGNMENT_TABLEPRINTER_H


#include "../TableCommand.h"

class TablePrinter : public TableCommand {
public:
    explicit TablePrinter(TableHandler &handler);

    void RunCommand() override;

    void printTable(int index);
};


#endif //SECOND_ASSIGNMENT_TABLEPRINTER_H
