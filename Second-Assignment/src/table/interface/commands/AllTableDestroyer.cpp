//
// Created by Miłosz Korman on 23.10.18.
//

#include "AllTableDestroyer.h"

AllTableDestroyer::AllTableDestroyer(TableHandler &handler) : TableDestroyer(handler) {
}

void AllTableDestroyer::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    for (int i = 0; i < tables->size(); ++i)
        delete (*tables)[i];

    tables->clear();
}
