//
// Created by Miłosz Korman on 23.10.18.
//

#include "AllTablePrinter.h"

AllTablePrinter::AllTablePrinter(TableHandler &handler) : TablePrinter(handler) {
}

void AllTablePrinter::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    for (int i = 0; i < tables->size(); i++)
        printTable(i);
}
