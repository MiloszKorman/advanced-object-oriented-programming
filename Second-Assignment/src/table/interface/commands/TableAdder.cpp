//
// Created by Miłosz Korman on 23.10.18.
//

#include "TableAdder.h"
#include "../../../../lib/utils/Utils.h"

TableAdder::TableAdder(TableHandler &handler) : TableCommand(handler) {
}

void TableAdder::RunCommand() {
    std::cout << SUPPLY_PARAMETERS;
    if (Utils::getBoolean())
        addParameterizedTable();
    else
        addDefaultTable();
}

void TableAdder::addDefaultTable() {
    handler->getTables().push_back(new Table);
}

void TableAdder::addParameterizedTable() {
    std::cout << GET_TABLE_NAME;
    std::string name = Utils::getString();
    std::cout << GET_TABLE_LENGTH << std::endl;
    int nOfFields = Utils::getIntFrom(1);
    handler->getTables().push_back(new Table(name, nOfFields));
}
