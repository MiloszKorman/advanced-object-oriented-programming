//
// Created by Miłosz Korman on 23.10.18.
//

#include "TablePrinter.h"
#include "../../../../lib/utils/Utils.h"

TablePrinter::TablePrinter(TableHandler &handler) : TableCommand(handler) {
}

void TablePrinter::RunCommand() {
    std::vector<Table *> *tables = &handler->getTables();
    if (tables->empty())
        std::cout << EMPTY_LIST << std::endl;
    else {
        std::cout << GET_INDEX << std::endl;
        int index = Utils::getIntBetween(0, tables->size() - 1);
        printTable(index);
    }
}

void TablePrinter::printTable(int index) {
    std::cout << index << ":\t" << handler->getTables()[index]->toString() << std::endl;
}
