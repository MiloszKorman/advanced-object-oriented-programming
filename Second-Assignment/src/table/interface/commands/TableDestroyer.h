//
// Created by Miłosz Korman on 23.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLEDESTROYER_H
#define SECOND_ASSIGNMENT_TABLEDESTROYER_H


#include "../TableCommand.h"

class TableDestroyer : public TableCommand {
public:
    explicit TableDestroyer(TableHandler &handler);

    void RunCommand() override;
};


#endif //SECOND_ASSIGNMENT_TABLEDESTROYER_H
