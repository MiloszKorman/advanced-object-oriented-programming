//
// Created by Miłosz Korman on 22.10.18.
//

#ifndef SECOND_ASSIGNMENT_TABLEHANDLER_H
#define SECOND_ASSIGNMENT_TABLEHANDLER_H


#define DESTRUCTOR_MESSAGE "### TableHandler Destructor ###"

#include <vector>
#include "Table.h"

class TableHandler {
public:
    ~TableHandler();

    std::vector<Table *> &getTables();

private:
    std::vector<Table *> tables;
};


#endif //SECOND_ASSIGNMENT_TABLEHANDLER_H
