//
// Created by Miłosz Korman on 06.10.18.
//

#ifndef FIRST_ASSIGNMENT_CTABLE_H
#define FIRST_ASSIGNMENT_CTABLE_H

#define DEFAULT_NAME "Default Table"
#define DEFAULT_LENGTH 10
#define DOUBLE 2
#define CREATING_DEFAULT "### default: "
#define CREATING_PARAMETERED "### parametered: "
#define CREATING_COPIED "### copied: "
#define DELETING "### deleting: "
#define TRIPLE_HASH " ###"
#define VALUES " values: "
#define LENGTH " len: "
#define COPY "_copy"


#include <string>
#include <iostream>
#include <cstring>
#include <sstream>

class Table {
public:
    //Constructors
    Table();

    Table(std::string name, int tableLength);

    Table(Table &pcOther); //Copying constructor

    //Destructor
    ~Table();

    //Methods
    std::string getName();

    void setName(std::string newName);

    int getLength();

    bool setLength(int newLength);

    int getElement(int index, bool *success);

    bool setElement(int index, int value);

    Table *clone();

    Table &operator=(Table &pcOther);

    std::string toString();

    void doubleArr();

private:
    //Variables
    std::string name;
    int tableLength;
    int *array;

    void populateWithZeros(int start, int finish);

    bool checkRange(int index, int start, int finish);
};


#endif //FIRST_ASSIGNMENT_CTABLE_H
