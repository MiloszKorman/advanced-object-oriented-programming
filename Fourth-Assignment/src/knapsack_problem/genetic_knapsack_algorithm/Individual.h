//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef FOURTH_ASSIGNMENT_INDIVIDUAL_H
#define FOURTH_ASSIGNMENT_INDIVIDUAL_H


#include <vector>
#include "../KnapsackProblem.h"

class Individual {
public:

    Individual(std::vector<int> *genotype, KnapsackProblem *knapsackProblem);

    Individual(Individual &individual);

    ~Individual();

    int estimateFitness();

    void mutate(const double mutProb);

    std::vector<Individual *> *cross(const Individual &otherParent);

    std::vector<int> *getGenotype();

private:
    std::vector<int> *genotype;
    KnapsackProblem *knapsackProblem;
};


#endif //FOURTH_ASSIGNMENT_INDIVIDUAL_H
