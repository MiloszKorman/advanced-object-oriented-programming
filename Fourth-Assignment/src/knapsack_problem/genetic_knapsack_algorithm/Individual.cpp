//
// Created by Miłosz Korman on 22.11.18.
//

#include <random>
#include "Individual.h"

using namespace std;

Individual::Individual(vector<int> *genotype, KnapsackProblem *knapsackProblem) {
    this->genotype = genotype;
    this->knapsackProblem = knapsackProblem;
}

Individual::Individual(Individual &individual) {
    this->genotype = new vector<int>();
    *(this->genotype) = *(individual.genotype);
    this->knapsackProblem = individual.knapsackProblem;
}

Individual::~Individual() {
    delete genotype;
}

int Individual::estimateFitness() {
    int fitness;

    int individualsSize = 0;
    int individualsFitness = 0;
    vector<Item *> *items = knapsackProblem->getItems();
    for (int i = 0; i < genotype->size(); i++) {
        if (genotype->at(i) == 1) {
            Item *item = items->at(i);
            individualsFitness += item->getValue();
            individualsSize += item->getWeight();
        }
    }

    fitness = (individualsSize <= knapsackProblem->getBagCapacity()) ? individualsFitness : 0;

    return fitness;
}

void Individual::mutate(const double mutProb) {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, std::nextafter(1.0, std::numeric_limits<double>::max()));

    for (int i = 0; i < genotype->size(); i++) {
        if (dis(gen) < mutProb) {
            if (genotype->at(i) == 1) {
                (*genotype)[i] = 0;
            } else {
                (*genotype)[i] = 1;
            }
        }
    }
}

std::vector<Individual *> *Individual::cross(const Individual &otherParent) {

    if (this->genotype->size() != otherParent.genotype->size()) {
        return new vector<Individual *>(0);
    }

    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(1, this->genotype->size() - 1);
    int crossPoint = dis(gen);

    vector<int> *genotype1 = new vector<int>(this->genotype->size());
    vector<int> *genotype2 = new vector<int>(this->genotype->size());

    for (int i = 0; i < crossPoint; i++) {
        (*genotype1)[i] = (*this->genotype)[i];
        (*genotype2)[i] = (*otherParent.genotype)[i];
    }

    for (int i = crossPoint; i < this->genotype->size(); i++) {
        (*genotype1)[i] = (*otherParent.genotype)[i];
        (*genotype2)[i] = (*this->genotype)[i];
    }

    return new vector<Individual *>{
            new Individual(genotype1, this->knapsackProblem),
            new Individual(genotype2, this->knapsackProblem)
    };
}

vector<int> *Individual::getGenotype() {
    return genotype;
}
