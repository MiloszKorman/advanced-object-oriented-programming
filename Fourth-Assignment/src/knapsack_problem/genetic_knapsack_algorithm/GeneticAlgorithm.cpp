//
// Created by Miłosz Korman on 22.11.18.
//

#include <random>
#include "GeneticAlgorithm.h"

using namespace std;

GeneticAlgorithm::GeneticAlgorithm(int popSize, double crossProb, double mutProb, KnapsackProblem &knapsackProblem) {
    
    if (popSize >= MIN_POP_SIZE) this->popSize = popSize;
    else this->popSize = MIN_POP_SIZE;
    
    if (0 <= crossProb && crossProb <= 1) this->crossProb = crossProb;
    else this->crossProb = DEFAULT_PROBABILITY;

    if (0 <= mutProb && mutProb <= 1) this->mutProb = mutProb;
    else this->mutProb = DEFAULT_PROBABILITY;
    
    this->nOfItems = knapsackProblem.getItems()->size();
    this->knapsackProblem = &knapsackProblem;
}

Individual *GeneticAlgorithm::solve(int nOfIterations) {
    
    if (nOfIterations < 1) nOfIterations = 1;
    
    vector<Individual *> *population = generateInitialPopulation();

    for (int i = 0; i < nOfIterations; i++) {
        vector<Individual *> *nextPopulation = new vector<Individual *>();

        while(nextPopulation->size() < popSize) {
            addChildrenOrClones(findTwoAndChooseBetter(population), findTwoAndChooseBetter(population), nextPopulation);
        }

        deletePopulation(population);
        population = nextPopulation;

        mutatePopulation(population);
    }

    return findBestDeleteRest(population);
}

vector<Individual *> *GeneticAlgorithm::generateInitialPopulation() {
    vector<Individual *> *initialPopulation = new vector<Individual *>();

    for (int i = 0; i < popSize; i++) {
        initialPopulation->push_back(generateIndividual());
    }

    return initialPopulation;
}

Individual *GeneticAlgorithm::generateIndividual() {
    vector<int> *genotype = new vector<int>(nOfItems);

    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(0, 1);

    for (int i = 0; i < genotype->size(); i++) {
        (*genotype)[i] = dis(gen);
    }

    return new Individual(genotype, knapsackProblem);
}

Individual *GeneticAlgorithm::findBestDeleteRest(vector<Individual *> *population) {
    int bestIndex = 0;
    Individual *best = findBest(bestIndex, population);

    population->at(bestIndex) = NULL; //Don't delete the best one
    deletePopulation(population);

    return best;
}

Individual *GeneticAlgorithm::findBest(int &index, std::vector<Individual *> *population) {
    Individual *best = (*population)[0];

    for (int i = 1; i < population->size(); i++) {
        if (population->at(i)->estimateFitness() > best->estimateFitness()) {
            best = (*population)[i]; //set new best
            index = i; //set new bests index
        }
    }

    return best;
}

void GeneticAlgorithm::mutatePopulation(vector<Individual *> *population) {
    for (Individual *individual : *population)  {
        if (toMutate()) {
            individual->mutate(mutProb);
        }
    }
}

void GeneticAlgorithm::deletePopulation(vector<Individual *> *population) {
    for (Individual *individual : *population) {
        delete individual;
    }

    delete population;
}

void GeneticAlgorithm::addChildrenOrClones(Individual *parent1, Individual *parent2, vector<Individual *> *population) {
    if (toCross()) {
        vector<Individual *> *children = parent1->cross(*parent2);
        Individual *firstChild = children->at(0);
        Individual *secondChild = children->at(1);

        delete children; //don't need this vector anymore

        if (population->size() + 1 < popSize) {
            //add both children
            population->push_back(firstChild);
            population->push_back(secondChild);
        } else {
            //add only the better child
            population->push_back(chooseBetterDeleteOther(firstChild, secondChild));
        }
    } else {
        if (population->size() + 1 < popSize) {
            //add both parents
            population->push_back(new Individual(*parent1));
            population->push_back(new Individual(*parent2));
        } else {
            //have to only add the better parent
            population->push_back(new Individual(*chooseBetter(parent1, parent2)));
        }
    }
}

Individual *GeneticAlgorithm::findTwoAndChooseBetter(vector<Individual *> *population) {
    return chooseBetter(findRandom(population), findRandom(population));
}

Individual *GeneticAlgorithm::findRandom(vector<Individual *> *population) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, population->size() - 1);

    return population->at(dis(gen));
}

Individual *GeneticAlgorithm::chooseBetter(Individual *ind1, Individual *ind2) {
    if (ind1->estimateFitness() < ind2->estimateFitness())
        return ind2;
    else
        return ind1;
}

Individual *GeneticAlgorithm::chooseBetterDeleteOther(Individual *ind1, Individual *ind2) {
    if (ind1->estimateFitness() < ind2->estimateFitness()) {
        delete ind1;
        return ind2;
    } else {
        delete ind2;
        return ind1;
    }
}

bool GeneticAlgorithm::toCross() {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, std::nextafter(1.0, std::numeric_limits<double>::max()));

    return dis(gen) < crossProb;
}

bool GeneticAlgorithm::toMutate() {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, std::nextafter(1.0, std::numeric_limits<double>::max()));

    return dis(gen) < mutProb;
}
