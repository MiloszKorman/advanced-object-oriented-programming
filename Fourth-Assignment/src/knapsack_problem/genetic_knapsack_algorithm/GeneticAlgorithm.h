//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef FOURTH_ASSIGNMENT_GENETICALGORITHM_H
#define FOURTH_ASSIGNMENT_GENETICALGORITHM_H


#define MIN_POP_SIZE 2

#define DEFAULT_PROBABILITY 0.5

#include "../KnapsackProblem.h"
#include "Individual.h"

class GeneticAlgorithm {
public:
    GeneticAlgorithm(int popSize, double crossProb, double mutProb, KnapsackProblem &knapsackProblem);

    Individual *solve(int nOfIterations);

private:
    int popSize;
    double crossProb;
    double mutProb;
    int nOfItems;
    KnapsackProblem *knapsackProblem;

    std::vector<Individual *> *generateInitialPopulation();
    Individual *generateIndividual();
    Individual *findBestDeleteRest(std::vector<Individual *> *population);
    Individual *findBest(int &index, std::vector<Individual *> *population);
    void mutatePopulation(std::vector<Individual *> *population);
    void deletePopulation(std::vector<Individual *> *population);
    void addChildrenOrClones(Individual *parent1, Individual *parent2, std::vector<Individual *> *population);
    Individual *findTwoAndChooseBetter(std::vector<Individual *> *population);
    Individual *findRandom(std::vector<Individual *> *population);
    Individual *chooseBetter(Individual *ind1, Individual *ind2);
    Individual *chooseBetterDeleteOther(Individual *ind1, Individual *ind2);
    bool toCross();
    bool toMutate();
};


#endif //FOURTH_ASSIGNMENT_GENETICALGORITHM_H
