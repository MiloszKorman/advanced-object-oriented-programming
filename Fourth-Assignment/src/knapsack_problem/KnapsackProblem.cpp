//
// Created by Miłosz Korman on 22.11.18.
//

#include "KnapsackProblem.h"

using namespace std;

KnapsackProblem::KnapsackProblem(vector<Item *> &items, int capacity) {
    this->items = &items;

    if (capacity > 0) this->capacity = capacity;
    else this->capacity = 0;
}

KnapsackProblem::KnapsackProblem(KnapsackProblem &knapsackProblem) {
    *this->items = *knapsackProblem.items;
    this->capacity = knapsackProblem.capacity;
}

int KnapsackProblem::getBagCapacity() const {
    return capacity;
}

std::vector<Item *> *KnapsackProblem::getItems() {
    return items;
}

std::vector<Item *> *KnapsackProblem::solution(std::vector<int> *genotype) const {

    vector<Item *> *solution = new vector<Item *>();

    for (int i = 0; i < genotype->size(); i++) {
        if (genotype->at(i) == 1) {
            solution->push_back(items->at(i));
        }
    }

    return solution;
}
