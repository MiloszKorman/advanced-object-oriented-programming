//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef FOURTH_ASSIGNMENT_KNAPSACKPROBLEM_H
#define FOURTH_ASSIGNMENT_KNAPSACKPROBLEM_H


#include <vector>
#include "Item.h"

class KnapsackProblem {
public:
    KnapsackProblem(std::vector<Item *> &items, int capacity);

    KnapsackProblem(KnapsackProblem &knapsackProblem);

    int getBagCapacity() const;

    std::vector<Item *> *getItems();

    std::vector<Item *> *solution(std::vector<int> *genotype) const;

private:
    std::vector<Item *> *items;
    int capacity;
};


#endif //FOURTH_ASSIGNMENT_KNAPSACKPROBLEM_H
