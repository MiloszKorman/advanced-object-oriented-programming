#include <iostream>
#include <vector>
#include "knapsack_problem/Item.h"
#include "knapsack_problem/KnapsackProblem.h"
#include "knapsack_problem/genetic_knapsack_algorithm/GeneticAlgorithm.h"

using namespace std;

int main() {
    vector<Item *> items{
        new Item("item 1", 2, 4),
        new Item("item 2", 9, 1),
        new Item("item 3", 1, 10),
        new Item("item 4", 8, 12),
        new Item("item 5", 4, 13),
        new Item("item 6", 20, 100),
        new Item("item 7", 4, 19),
        new Item("item 8", 32, 13),
        new Item("item 9", 6, 22),
        new Item("item 10", 3, 8)
        };

    KnapsackProblem knapsackProblem(items, 34);
    GeneticAlgorithm geneticAlgorithm(10, 0.75, 0.25, knapsackProblem);

    Individual *best = geneticAlgorithm.solve(1000);
    vector<Item *> *solution = knapsackProblem.solution(best->getGenotype());

    cout << "Solution: " << endl;

    //Expected: 3 6 7 9 10 ? valueSum=159 weightSum=34
    int valueSum = 0;
    int weightSum = 0;
    for (Item *item : *solution) {
        cout << *item << endl;
        valueSum += item->getValue();
        weightSum += item->getWeight();
    }
    cout << "Value sum: " << valueSum << endl;
    cout << "Weight sum: " << weightSum << endl;

    for (Item *item : items) {
        delete item;
    }

    delete solution;
    delete best;

    return 0;
}