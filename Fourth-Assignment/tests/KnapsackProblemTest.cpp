//
// Created by Miłosz Korman on 24.11.18.
//

#include "../lib/catch.hpp"
#include "../src/knapsack_problem/KnapsackProblem.h"

using namespace std;

SCENARIO("Solution test") {
    GIVEN("A knapsack_problem with items") {
        int KNAPSACK_SIZE = 10;
        Item ITEM1("item 1", 3, 2);
        Item ITEM2("item 2", 4, 12);
        Item ITEM3("item 3", 3, 15);
        Item ITEM4("item 4", 20, 30);
        vector<Item *> KNAPSACK_ITEMS{&ITEM1, &ITEM2, &ITEM3, &ITEM4};

        KnapsackProblem KNAPSACK_PROBLEM(KNAPSACK_ITEMS, KNAPSACK_SIZE);

        vector<int> BIT_SOLUTION = {1, 1, 1, 0};

        WHEN("solution is called with 0/1 representation of solution") {

            vector<Item *> *solution = KNAPSACK_PROBLEM.solution(&BIT_SOLUTION);

            THEN("vector of adequate items is returned") {
                vector<Item *> EXPECTED_SOLUTION{&ITEM1, &ITEM2, &ITEM3};

                REQUIRE(*solution == EXPECTED_SOLUTION);

                AND_THEN("Delete solution") {
                    delete solution;
                }

            }
        }
    }
}