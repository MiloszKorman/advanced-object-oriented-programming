//
// Created by Miłosz Korman on 24.11.18.
//

#include "../src/knapsack_problem/genetic_knapsack_algorithm/Individual.h"
#include "../lib/catch.hpp"
#include "../src/knapsack_problem/KnapsackProblem.h"

using namespace std;

SCENARIO("Estimate fitness test") {
    GIVEN("Knapsack") {
        int KNAPSACK_SIZE = 5;
        vector<Item *> KNAPSACK_ITEMS;
        Item *ITEM1 = new Item("item 1", 3, 2);
        Item *ITEM2 = new Item("item 2", 6, 12);
        Item *ITEM3 = new Item("item 3", 10, 30);
        KNAPSACK_ITEMS.push_back(ITEM1);
        KNAPSACK_ITEMS.push_back(ITEM2);
        KNAPSACK_ITEMS.push_back(ITEM3);

        KnapsackProblem KNAPSACK_PROBLEM(KNAPSACK_ITEMS, KNAPSACK_SIZE);

        AND_GIVEN("Not fitting individual") {
            vector<int> *GENOTYPE = new vector<int>{0, 1, 1};

            Individual individual(GENOTYPE, &KNAPSACK_PROBLEM);

            WHEN("Fitness is calculated") {
                int individualFitness = individual.estimateFitness();

                THEN("It should be zero") {
                    REQUIRE(individualFitness == 0);

                    AND_THEN("Clean items") {
                        for (Item *item : KNAPSACK_ITEMS) {
                            delete item;
                        }
                    }
                }
            }
        }

        AND_GIVEN("Fitting individual") {
            vector<int> *GENOTYPE = new vector<int>{1, 0, 0};

            Individual individual(GENOTYPE, &KNAPSACK_PROBLEM);

            WHEN("Fitness is calculated") {
                int individualsFitness = individual.estimateFitness();

                THEN("It should be adequate") {
                    REQUIRE(individualsFitness == 2);

                    AND_THEN("Clean items") {
                        for (Item *item : KNAPSACK_ITEMS) {
                            delete item;
                        }
                    }
                }
            }
        }
    }

    GIVEN("Knapsack with few suitable") {
        int KNAPSACK_SIZE = 10;
        vector<Item *> KNAPSACK_ITEMS;
        Item *ITEM1 = new Item("item 1", 3, 2);
        Item *ITEM2 = new Item("item 2", 4, 12);
        Item *ITEM3 = new Item("item 3", 3, 15);
        Item *ITEM4 = new Item("item 4", 20, 30);
        KNAPSACK_ITEMS.push_back(ITEM1);
        KNAPSACK_ITEMS.push_back(ITEM2);
        KNAPSACK_ITEMS.push_back(ITEM3);
        KNAPSACK_ITEMS.push_back(ITEM4);

        KnapsackProblem KNAPSACK_PROBLEM(KNAPSACK_ITEMS, KNAPSACK_SIZE);

        AND_GIVEN("Fitting individual") {
            vector<int> *GENOTYPE = new vector<int>{1, 1, 1, 0};

            Individual individual(GENOTYPE, &KNAPSACK_PROBLEM);

            WHEN("Fitness is calculated") {
                int individualsFitness = individual.estimateFitness();

                THEN("It should be adequate") {
                    REQUIRE(individualsFitness == 29);

                    AND_THEN("Clean items") {
                        for (Item *item : KNAPSACK_ITEMS) {
                            delete item;
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("Mutate test") {
    GIVEN("An individual") {
        int KNAPSACK_SIZE = 10;
        vector<Item *> KNAPSACK_ITEMS;
        Item *ITEM1 = new Item("item 1", 3, 2);
        Item *ITEM2 = new Item("item 2", 4, 12);
        Item *ITEM3 = new Item("item 3", 3, 15);
        Item *ITEM4 = new Item("item 4", 20, 30);
        KNAPSACK_ITEMS.push_back(ITEM1);
        KNAPSACK_ITEMS.push_back(ITEM2);
        KNAPSACK_ITEMS.push_back(ITEM3);
        KNAPSACK_ITEMS.push_back(ITEM4);

        KnapsackProblem KNAPSACK_PROBLEM(KNAPSACK_ITEMS, KNAPSACK_SIZE);

        vector<int> *GENOTYPE = new vector<int>{1, 1, 1, 0};

        Individual individual(GENOTYPE, &KNAPSACK_PROBLEM);

        WHEN("He is fully mutated") {
            individual.mutate(1.1); //apparently, random number [0, 1] needs to be SMALLER than mutProb, which is [0,1]

            THEN("His new genotype should be the opposite of an old one") {
                vector<int> NEW_GENOTYPE = {0, 0, 0, 1};
                REQUIRE(*(individual.getGenotype()) == NEW_GENOTYPE);

                AND_THEN("Clean items") {
                    for (Item *item : KNAPSACK_ITEMS) {
                        delete item;
                    }
                }
            }
        }

        WHEN("He isn't mutated") {
            individual.mutate(0);

            THEN("His new genotype should be the opposite of an old one") {
                REQUIRE(*(individual.getGenotype()) == *GENOTYPE);

                AND_THEN("Clean items") {
                    for (Item *item : KNAPSACK_ITEMS) {
                        delete item;
                    }
                }
            }
        }
    }
}

SCENARIO("Cross test") {
    GIVEN("A knapsack_problem") {
        int KNAPSACK_SIZE = 10;
        vector<Item *> KNAPSACK_ITEMS;
        Item *ITEM1 = new Item("item 1", 3, 2);
        Item *ITEM2 = new Item("item 2", 4, 12);
        Item *ITEM3 = new Item("item 3", 3, 15);
        Item *ITEM4 = new Item("item 4", 20, 30);
        KNAPSACK_ITEMS.push_back(ITEM1);
        KNAPSACK_ITEMS.push_back(ITEM2);
        KNAPSACK_ITEMS.push_back(ITEM3);
        KNAPSACK_ITEMS.push_back(ITEM4);

        KnapsackProblem KNAPSACK_PROBLEM(KNAPSACK_ITEMS, KNAPSACK_SIZE);

        AND_GIVEN("Two same individuals") {
            vector<int> GENOTYPE = {1, 1, 1, 1};

            vector<int> *GENOTYPE1 = new vector<int>{1, 1, 1, 1};
            vector<int> *GENOTYPE2 = new vector<int>{1, 1, 1, 1};

            Individual individual1(GENOTYPE1, &KNAPSACK_PROBLEM);
            Individual individual2(GENOTYPE2, &KNAPSACK_PROBLEM);

            WHEN("They cross") {
                vector<Individual *> *children = individual1.cross(individual2);

                THEN("Children genotype is same as parents") {

                    REQUIRE(children->size() == 2);

                    for (Individual *child : *children) {
                        REQUIRE(*(child->getGenotype()) == GENOTYPE);
                    }

                    AND_THEN("Clean items") {
                        for (Item *item : KNAPSACK_ITEMS) {
                            delete item;
                        }

                        AND_THEN("Clean children and their vector") {
                            for (Individual *child : *children) {
                                delete child;
                            }
                            delete children;
                        }
                    }
                }
            }
        }

        AND_GIVEN("Two different individuals") {
            vector<int> *GENOTYPE_1 = new vector<int>{1, 1, 0, 0};
            vector<int> *GENOTYPE_2 = new vector<int>{0, 0, 1, 1};

            Individual individual1(GENOTYPE_1, &KNAPSACK_PROBLEM);
            Individual individual2(GENOTYPE_2, &KNAPSACK_PROBLEM);

            WHEN("They cross") {
                vector<Individual *> *children = individual1.cross(individual2);

                THEN("Children genotypes are different than parents") {
                    REQUIRE(children->size() == 2);

                    for (Individual *child : *children) {
                        REQUIRE(*(child->getGenotype()) != *GENOTYPE_1);
                        REQUIRE(*(child->getGenotype()) != *GENOTYPE_2);
                    }

                    AND_THEN("Clean items") {
                        for (Item *item : KNAPSACK_ITEMS) {
                            delete item;
                        }

                        AND_THEN("Clean children and their vector") {
                            for (Individual *child : *children) {
                                delete child;
                            }
                            delete children;
                        }
                    }
                }
            }
        }
    }
}
