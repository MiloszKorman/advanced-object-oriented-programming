//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef FOURTH_ASSIGNMENT_INDIVIDUAL_H
#define FOURTH_ASSIGNMENT_INDIVIDUAL_H


#include <vector>
#include <random>
#include "../KnapsackProblem.h"

template <typename T>
class Individual {
public:

    Individual(KnapsackProblem<T> *knapsackProblem, double mutProb);

    Individual(std::vector<T> *genotype, KnapsackProblem<T> *knapsackProblem, double mutProb);

    Individual(Individual<T> &individual);

    ~Individual();

    int estimateFitness();

    void mutate();

    std::vector<Individual<T> *> *cross(const Individual<T> &otherParent) const;

    std::vector<T> *getGenotype();

    Individual &operator++();

    Individual operator++(int);

    Individual *operator+(const Individual<T> &otherParent) const;

    bool operator==(const Individual<T> &rhs) const;

    bool operator!=(const Individual<T> &rhs) const;

private:
    std::vector<T> *genotype;
    double mutProb;
    KnapsackProblem<T> *knapsackProblem;

    void initializeGenotype();
    T randomGenotypeElement(int index);
    void mutateGenotypeElement(int index);
};

template <typename T>
Individual<T>::Individual(KnapsackProblem<T> *knapsackProblem, double mutProb) {
    this->knapsackProblem = knapsackProblem;
    this->mutProb = mutProb;
    this->genotype = new vector<T>();
    initializeGenotype();
}

template <typename T>
void Individual<T>::mutate() {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, std::nextafter(1.0, std::numeric_limits<double>::max()));

    for (int i = 0; i < genotype->size(); i++) {
        if (dis(gen) < mutProb) {
            mutateGenotypeElement(i);
        }
    }
}

template <typename T>
std::vector<Individual<T> *> *Individual<T>::cross(const Individual<T> &otherParent) const {

    if (this->genotype->size() != otherParent.genotype->size()) {
        return new vector<Individual *>(0);
    }

    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(1, this->genotype->size() - 1);
    int crossPoint = dis(gen);

    vector<T> *genotype1 = new vector<T>(this->genotype->size());
    vector<T> *genotype2 = new vector<T>(this->genotype->size());

    for (int i = 0; i < crossPoint; i++) {
        (*genotype1)[i] = (*this->genotype)[i];
        (*genotype2)[i] = (*otherParent.genotype)[i];
    }

    for (int i = crossPoint; i < this->genotype->size(); i++) {
        (*genotype1)[i] = (*otherParent.genotype)[i];
        (*genotype2)[i] = (*this->genotype)[i];
    }

    return new vector<Individual *>{
            new Individual(genotype1, this->knapsackProblem, this->mutProb),
            new Individual(genotype2, this->knapsackProblem, this->mutProb)
    };
}

template <typename T>
vector<T> *Individual<T>::getGenotype() {
    return genotype;
}

template <typename T>
Individual<T> &Individual<T>::operator++() {
    this->mutate();

    return *this;
}

template <typename T>
Individual<T> Individual<T>::operator++(int) {
    Individual result(*this);
    ++(*this);
    return result;
}

template <typename T>
Individual<T> *Individual<T>::operator+(const Individual<T> &otherParent) const {
    vector<Individual *> *children = this->cross(otherParent);
    Individual *toReturn = children->at(0);
    delete children->at(1);
    delete children;
    return toReturn;
}

template <typename T>
bool Individual<T>::operator==(const Individual<T> &rhs) const {
    return *genotype == *rhs.genotype;
}

template <typename T>
bool Individual<T>::operator!=(const Individual<T> &rhs) const {
    return !(rhs == *this);
}

template <typename T>
void Individual<T>::initializeGenotype() {
    for (int i = 0; i < knapsackProblem->getItems()->size(); i++)
        genotype->push_back(randomGenotypeElement(i));
}

template <typename T>
void Individual<T>::mutateGenotypeElement(int index) {
    this->genotype->at(index) = randomGenotypeElement(index);
}

template <typename T>
Individual<T>::Individual(vector<T> *genotype, KnapsackProblem<T> *knapsackProblem, double mutProb) {
    this->genotype = genotype;
    this->knapsackProblem = knapsackProblem;
    this->mutProb = mutProb;
}

template <typename T>
Individual<T>::Individual(Individual<T> &individual) {
    this->genotype = new vector<T>();
    *(this->genotype) = *(individual.genotype);
    this->knapsackProblem = individual.knapsackProblem;
    this->mutProb = individual.mutProb;
}

template <typename T>
Individual<T>::~Individual() {
    delete genotype;
}

#endif //FOURTH_ASSIGNMENT_INDIVIDUAL_H
