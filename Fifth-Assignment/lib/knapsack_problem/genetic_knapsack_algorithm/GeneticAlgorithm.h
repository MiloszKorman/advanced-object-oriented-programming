//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef FOURTH_ASSIGNMENT_GENETICALGORITHM_H
#define FOURTH_ASSIGNMENT_GENETICALGORITHM_H


#define MIN_POP_SIZE 2
#define DEFAULT_PROBABILITY 0.5

#include "../KnapsackProblem.h"
#include "Individual.h"

template <typename T>
class GeneticAlgorithm {
public:
    GeneticAlgorithm(int popSize, double crossProb, double mutProb, KnapsackProblem<T> &knapsackProblem);

    Individual<T> *solve(long timeInS);

private:
    int popSize;
    double crossProb;
    double mutProb;
    int nOfItems;
    KnapsackProblem<T> *knapsackProblem;

    std::vector<Individual<T> *> *generateInitialPopulation();
    Individual<T> *findBestDeleteRest(std::vector<Individual<T> *> *population);
    Individual<T> *findBest(int &index, std::vector<Individual<T> *> *population);
    void mutatePopulation(std::vector<Individual<T> *> *population);
    void deletePopulation(std::vector<Individual<T> *> *population);
    void addChildrenOrClones(Individual<T> *parent1, Individual<T> *parent2, std::vector<Individual<T> *> *population);
    Individual<T> *findTwoAndChooseBetter(std::vector<Individual<T> *> *population);
    Individual<T> *findRandom(std::vector<Individual<T> *> *population);
    Individual<T> *chooseBetter(Individual<T> *ind1, Individual<T> *ind2);
    Individual<T> *chooseBetterDeleteOther(Individual<T> *ind1, Individual<T> *ind2);
    bool toCross();
    bool toMutate();
    bool isNextPopulationWorse(std::vector<Individual<T> *> *current, std::vector<Individual<T> *> *next);
};

#include "GeneticAlgorithm.cpp"

#endif //FOURTH_ASSIGNMENT_GENETICALGORITHM_H
