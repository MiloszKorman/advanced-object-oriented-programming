//
// Created by Miłosz Korman on 22.11.18.
//


#include "Individual.h"

using namespace std;

template <>
int Individual<bool>::estimateFitness() {
    int indiSize = 0;
    int indiFitness = 0;
    vector<Item *> *items = knapsackProblem->getItems();
    for (int i = 0; i < genotype->size(); i++)
        if (genotype->at(i)) {
            Item *item = items->at(i);
            indiFitness += item->getValue();
            indiSize += item->getWeight();
        }

    return (indiSize <= knapsackProblem->getBagCapacity()) ? indiFitness : 0;
}

template <>
int Individual<int>::estimateFitness() {
    int indiSize = 0;
    int indiFitness = 0;
    vector<Item *> *items = knapsackProblem->getItems();
    for (int i = 0; i < genotype->size(); i++) {
        Item *item = items->at(i);
        indiFitness += item->getValue() * genotype->at(i);
        indiSize += item->getWeight() * genotype->at(i);
    }

    return (indiSize <= knapsackProblem->getBagCapacity()) ? indiFitness : 0;
}

template <>
int Individual<double>::estimateFitness() {
    double indiSize = 0;
    double indiFitness = 0;
    vector<Item *> *items = knapsackProblem->getItems();
    for (int i = 0; i < genotype->size(); i++) {
        Item *item = items->at(i);
        indiFitness += genotype->at(i) * item->getValue();
        indiSize += genotype->at(i) * item->getWeight();
    }

    return (indiSize <= (double) knapsackProblem->getBagCapacity()) ? (int) indiFitness : 0;
}

template <>
bool Individual<bool>::randomGenotypeElement(int index) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::bernoulli_distribution d(0.5);
    return d(gen);
}

template <>
int Individual<int>::randomGenotypeElement(int index) {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(
            -this->knapsackProblem->getItemUpperLimitList()->at(index),
            this->knapsackProblem->getItemUpperLimitList()->at(index));
    int toReturn = dis(gen);
    return (toReturn > 0) ? toReturn : 0;
}

template <>
double Individual<double>::randomGenotypeElement(int index) {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(
            -this->knapsackProblem->getItemUpperLimitList()->at(index),
            this->knapsackProblem->getItemUpperLimitList()->at(index));
    double toReturn = dis(gen);
    return (toReturn > 0) ? toReturn : 0;
}

template <>
void Individual<bool>::mutateGenotypeElement(int index) {
    this->genotype->at(index) = this->genotype->at(index)^true;
}
