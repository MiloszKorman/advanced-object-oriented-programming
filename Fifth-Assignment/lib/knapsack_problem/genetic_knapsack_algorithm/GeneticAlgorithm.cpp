//
// Created by Miłosz Korman on 22.11.18.
//

#include <random>
#include <iostream>
#include <chrono>
#include "GeneticAlgorithm.h"

using namespace std;
using namespace chrono;

template <typename T>
GeneticAlgorithm<T>::GeneticAlgorithm(int popSize, double crossProb, double mutProb, KnapsackProblem<T> &knapsackProblem) {
    
    if (popSize >= MIN_POP_SIZE) this->popSize = popSize;
    else this->popSize = MIN_POP_SIZE;
    
    if (0 <= crossProb && crossProb <= 1) this->crossProb = crossProb;
    else this->crossProb = DEFAULT_PROBABILITY;

    if (0 <= mutProb && mutProb <= 1) this->mutProb = mutProb;
    else this->mutProb = DEFAULT_PROBABILITY;
    
    this->nOfItems = knapsackProblem.getItems()->size();
    this->knapsackProblem = &knapsackProblem;
}

template <typename T>
Individual<T> *GeneticAlgorithm<T>::solve(long timeInS) {

    if (timeInS < 1) timeInS = 1;

    time_point<steady_clock> initTime = steady_clock::now();
    long timeElapsed = 0;
    
    vector<Individual<T> *> *population = generateInitialPopulation();

    while (timeElapsed < timeInS) {
        vector<Individual<T> *> *nextPopulation = new vector<Individual<T> *>();

        while(nextPopulation->size() < popSize) {
            addChildrenOrClones(findTwoAndChooseBetter(population), findTwoAndChooseBetter(population), nextPopulation);
        }

        mutatePopulation(nextPopulation);

        if (!isNextPopulationWorse(population, nextPopulation)) {
            deletePopulation(population);
            population = nextPopulation;
        } else {
            deletePopulation(nextPopulation);
        }

        timeElapsed = duration_cast<seconds>(steady_clock::now() - initTime).count();
    }

    return findBestDeleteRest(population);
}

template <typename T>
vector<Individual<T> *> *GeneticAlgorithm<T>::generateInitialPopulation() {
    vector<Individual<T> *> *initialPopulation = new vector<Individual<T> *>();

    for (int i = 0; i < popSize; i++) {
        initialPopulation->push_back(new Individual<T>(knapsackProblem, mutProb));
    }

    return initialPopulation;
}

template <typename T>
Individual<T> *GeneticAlgorithm<T>::findBestDeleteRest(vector<Individual<T> *> *population) {
    int bestIndex = 0;
    Individual<T> *best = findBest(bestIndex, population);

    population->at(bestIndex) = NULL; //Don't delete the best one
    deletePopulation(population);

    return best;
}

template <typename T>
Individual<T> *GeneticAlgorithm<T>::findBest(int &index, std::vector<Individual<T> *> *population) {
    Individual<T> *best = (*population)[0];

    for (int i = 1; i < population->size(); i++) {
        if (population->at(i)->estimateFitness() > best->estimateFitness()) {
            best = (*population)[i]; //set new best
            index = i; //set new bests index
        }
    }

    return best;
}

template <typename T>
void GeneticAlgorithm<T>::mutatePopulation(vector<Individual<T> *> *population) {
    for (Individual<T> *individual : *population) {
        //if (toMutate())
            (*individual)++;
    }
}

template <typename T>
void GeneticAlgorithm<T>::deletePopulation(vector<Individual<T> *> *population) {
    for (Individual<T> *individual : *population) {
        delete individual;
    }

    delete population;
}

template <typename T>
void GeneticAlgorithm<T>::addChildrenOrClones(Individual<T> *parent1, Individual<T> *parent2, vector<Individual<T> *> *population) {
    if (toCross()) {
        if (population->size() + 1 < popSize) {
            //add both children
            population->push_back(*parent1+*parent2);
            population->push_back(*parent2+*parent1);
        } else {
            //add only the better child
            population->push_back(chooseBetterDeleteOther(*parent1+*parent2, *parent2+*parent1));
        }
    } else {
        if (population->size() + 1 < popSize) {
            //add both parents
            population->push_back(new Individual<T>(*parent1));
            population->push_back(new Individual<T>(*parent2));
        } else {
            //have to only add the better parent
            population->push_back(new Individual<T>(*chooseBetter(parent1, parent2)));
        }
    }
}

template <typename T>
Individual<T> *GeneticAlgorithm<T>::findTwoAndChooseBetter(vector<Individual<T> *> *population) {
    return chooseBetter(findRandom(population), findRandom(population));
}

template <typename T>
Individual<T> *GeneticAlgorithm<T>::findRandom(vector<Individual<T> *> *population) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, population->size() - 1);

    return population->at(dis(gen));
}

template <typename T>
Individual<T> *GeneticAlgorithm<T>::chooseBetter(Individual<T> *ind1, Individual<T> *ind2) {
    if (ind1->estimateFitness() < ind2->estimateFitness())
        return ind2;
    else
        return ind1;
}

template <typename T>
Individual<T> *GeneticAlgorithm<T>::chooseBetterDeleteOther(Individual<T> *ind1, Individual<T> *ind2) {
    if (ind1->estimateFitness() < ind2->estimateFitness()) {
        delete ind1;
        return ind2;
    } else {
        delete ind2;
        return ind1;
    }
}

template <typename T>
bool GeneticAlgorithm<T>::toCross() {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, std::nextafter(1.0, std::numeric_limits<double>::max()));

    return dis(gen) < crossProb;
}

template <typename T>
bool GeneticAlgorithm<T>::toMutate() {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, std::nextafter(1.0, std::numeric_limits<double>::max()));

    return dis(gen) < mutProb;
}

template <typename T>
bool GeneticAlgorithm<T>::isNextPopulationWorse(std::vector<Individual<T> *> *current, std::vector<Individual<T> *> *next) {
    int dummyIndex = 0;
    Individual<T> *currentBest = findBest(dummyIndex, current);
    Individual<T> *nextBest = findBest(dummyIndex, next);
    currentBest->estimateFitness();
    nextBest->estimateFitness();
    //std::cout << currentBest->estimateFitness() << " " << nextBest->estimateFitness() << std::endl;
    return nextBest->estimateFitness() < currentBest->estimateFitness();
}
