//
// Created by Miłosz Korman on 22.11.18.
//


#include <cmath>
#include "KnapsackProblem.h"

using namespace std;

template <>
void KnapsackProblem<bool>::buildItemUpLimList() {
    for (int i = 0; i < items->size(); i++)
        itemUpLimList->push_back(1);
}

template <>
void KnapsackProblem<int>::buildItemUpLimList() {
    for (int i = 0; i < items->size(); i++)
        itemUpLimList->push_back(floor(this->capacity/items->at(i)->getWeight()));
}

template <>
void KnapsackProblem<double>::buildItemUpLimList() {
    for (Item *item : *items)
        itemUpLimList->push_back(this->capacity/(double)item->getWeight());
}
