//
// Created by Miłosz Korman on 22.11.18.
//

#include "Item.h"

using namespace std;

Item::Item(string name, int weight, int value) {
    this->name = name;
    this->weight = weight;
    this->value = value;
}

string Item::getName() const {
    return name;
}

int Item::getValue() const {
    return value;
}

int Item::getWeight() const {
    return weight;
}

ostream &operator<<(ostream &os, const Item &item) {
    os << "Name: " << item.name << " Value: " << item.value << " Weight: " << item.weight;
    return os;
}
