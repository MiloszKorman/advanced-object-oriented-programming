//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef FOURTH_ASSIGNMENT_KNAPSACKPROBLEM_H
#define FOURTH_ASSIGNMENT_KNAPSACKPROBLEM_H


#include <vector>
#include <tuple>
#include "Item.h"

using namespace std;

template <typename T>
class KnapsackProblem {
public:
    KnapsackProblem(std::vector<Item *> &items, double capacity);

    KnapsackProblem(KnapsackProblem &knapsackProblem);

    ~KnapsackProblem();

    int getBagCapacity() const;

    std::vector<double> *getItemUpperLimitList();

    std::vector<Item *> *getItems();

    std::vector<std::tuple<T, Item *>> *solution(std::vector<T> *genotype) const;

private:
    std::vector<Item *> *items;
    int capacity;
    std::vector<double> *itemUpLimList;

    void buildItemUpLimList();
};

template <typename T>
KnapsackProblem<T>::KnapsackProblem(vector<Item *> &items, double capacity) {
    this->items = &items;

    if (capacity > 0) this->capacity = capacity;
    else this->capacity = 0;

    this->itemUpLimList = new vector<double>();
    buildItemUpLimList();
}

template <typename T>
KnapsackProblem<T>::KnapsackProblem(KnapsackProblem &knapsackProblem) {
    this->items = knapsackProblem.items;
    this->capacity = knapsackProblem.capacity;
    *this->itemUpLimList = *knapsackProblem.itemUpLimList;
}

template <typename T>
KnapsackProblem<T>::~KnapsackProblem() {
    delete itemUpLimList;
}

template <typename T>
int KnapsackProblem<T>::getBagCapacity() const {
    return capacity;
}

template <typename T>
vector<Item *> *KnapsackProblem<T>::getItems() {
    return items;
}

template <typename T>
vector<tuple<T, Item *>> *KnapsackProblem<T>::solution(vector<T> *genotype) const {
    vector<tuple<T, Item *>> *result = new vector<tuple<T, Item *>>();

    for (int i = 0; i < items->size(); i++)
        result->push_back(make_tuple(genotype->at(i), items->at(i)));

    return result;
}

template<typename T>
vector<double> *KnapsackProblem<T>::getItemUpperLimitList() {
    return itemUpLimList;
}


#endif //FOURTH_ASSIGNMENT_KNAPSACKPROBLEM_H
