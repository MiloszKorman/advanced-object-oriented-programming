//
// Created by Miłosz Korman on 22.11.18.
//

#ifndef FOURTH_ASSIGNMENT_ITEM_H
#define FOURTH_ASSIGNMENT_ITEM_H


#include <string>
#include <ostream>

class Item {
public:
    Item(std::string name, int weight, int value);

    std::string getName() const;
    int getValue() const;
    int getWeight() const;

    friend std::ostream &operator<<(std::ostream &os, const Item &item);

private:
    std::string name;
    int value;
    int weight;
};


#endif //FOURTH_ASSIGNMENT_ITEM_H
