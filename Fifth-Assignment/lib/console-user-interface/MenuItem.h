//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_MENUITEM_H
#define SECOND_ASSIGNMENT_MENUITEM_H

#define DEFAULT_COMMAND "Default Command"
#define DEFAULT_NAME "Default Name"


#include <string>

class MenuItem {
public:
    MenuItem();

    MenuItem(const std::string &command, const std::string &name);

    virtual ~MenuItem() = default;

    virtual void Run() = 0;

    std::string getCommand() const;

    std::string getName() const;

    std::string toString() const;

protected:
    std::string command;
    std::string name;
};

#endif //SECOND_ASSIGNMENT_MENUITEM_H
