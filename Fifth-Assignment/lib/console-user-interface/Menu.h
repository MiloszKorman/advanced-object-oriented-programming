//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_MENU_H
#define SECOND_ASSIGNMENT_MENU_H

#define INTRODUCE_OPTIONS "\n----------Commands: ----------"
#define GET_USERS_CHOICE "Which one do you want to choose?: "
#define INCORRECT_INPUT "\nINCORRECT INPUT!"

#define END_RUN "back"

#define NEW_LINES "\n\n"

#include <vector>
#include "MenuItem.h"

class Menu : public MenuItem {
public:
    Menu();

    Menu(const std::string &command, const std::string &name);

    ~Menu() override;

    void Run() override;

    void addItem(MenuItem *item);

    void removeItem(std::string itemCommand);

private:
    std::vector<MenuItem *> items;

    bool chooseCommand(const std::string &commandName);
};


#endif //SECOND_ASSIGNMENT_MENU_H
