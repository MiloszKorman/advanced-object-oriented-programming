//
// Created by Miłosz Korman on 21.10.18.
//

#include <string>
#include <iostream>
#include "MenuItem.h"

MenuItem::MenuItem() {
    command = DEFAULT_COMMAND;
    name = DEFAULT_NAME;
}

MenuItem::MenuItem(const std::string &command, const std::string &name) {
    this->command = command;
    this->name = name;
}

std::string MenuItem::getCommand() const {
    return command;
};

std::string MenuItem::getName() const {
    return name;
};

std::string MenuItem::toString() const {
    return "| " + name + " (" + command + ")" + " |";
}
