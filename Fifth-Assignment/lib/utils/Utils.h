//
// Created by Miłosz Korman on 21.10.18.
//

#ifndef SECOND_ASSIGNMENT_UTILS_H
#define SECOND_ASSIGNMENT_UTILS_H


#define GET_INT_FROM "Provide an integer equal or greater than "
#define GET_INT_BETWEEN "Provide an integer between "
#define AND " and "
#define COLON ": "
#define GET_ANY_INTEGER "Provide an integer: "
#define GET_DOUBLE_FROM "Provide a double from "
#define GET_DOUBLE_BETWEEN "Provide a double between "

#include "../console-user-interface/MenuItem.h"

class Utils {
public:
    static std::string getString();

    static bool getBoolean();

    static int getIntFrom(int bottom);

    static int getIntBetween(int bottom, int top);

    static int getAnyInt();

    static double getDoubleFrom(double bottom);

    static double getDoubleBetween(double bottom, double top);
};


#endif //SECOND_ASSIGNMENT_UTILS_H
