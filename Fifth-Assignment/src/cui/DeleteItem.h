//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_DELETEITEM_H
#define FIFTH_ASSIGNMENT_DELETEITEM_H


#define GET_INDEX "At which index?: "

#include "KnapsackProblemCommand.h"


class DeleteItem : public KnapsackProblemCommand {
public:
    explicit DeleteItem(KnapsackProblemHandler *handler);

    void RunCommand() override;

};

#endif //FIFTH_ASSIGNMENT_DELETEITEM_H
