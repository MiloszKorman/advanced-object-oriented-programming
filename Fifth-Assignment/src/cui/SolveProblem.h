//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_SOLVEPROBLEM_H
#define FIFTH_ASSIGNMENT_SOLVEPROBLEM_H


#include "KnapsackProblemCommand.h"

template <typename T>
class SolveProblem : public KnapsackProblemCommand {
public:
    explicit SolveProblem(KnapsackProblemHandler *handler);

    void RunCommand() override;

private:
    void printSolution(vector<tuple<T, Item *>> *solution);
};

#include "SolveProblem.cpp"

#endif //FIFTH_ASSIGNMENT_SOLVEPROBLEM_H
