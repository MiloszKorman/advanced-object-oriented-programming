//
// Created by Miłosz Korman on 14.12.18.
//

#include <iostream>
#include "AddItem.h"
#include "../../lib/utils/Utils.h"


AddItem::AddItem(KnapsackProblemHandler *handler) : KnapsackProblemCommand(handler) {}

void AddItem::RunCommand() {
    this->handler->addItem(newItem());
}

Item *AddItem::newItem() {
    cout << GET_ITEMS_NAME << endl;
    string itemsName = Utils::getString();
    cout << endl;
    cout << GET_ITEMS_WEIGHT << endl;
    int itemsWeight = Utils::getIntFrom(1);
    cout << GET_ITEMS_VALUE << endl;
    int itemsValue = Utils::getIntFrom(0);
    return new Item(itemsName, itemsWeight, itemsValue);
}
