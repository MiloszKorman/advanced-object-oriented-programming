//
// Created by Miłosz Korman on 14.12.18.
//

#include <iostream>
#include "PrintProblem.h"


PrintProblem::PrintProblem(KnapsackProblemHandler *handler) : KnapsackProblemCommand(handler) {}

void PrintProblem::RunCommand() {
    cout << endl;
    vector<Item *> *items = handler->getItems();
    for (Item *item : *items) {
        cout << *item << endl;
    }
    cout << endl;
}
