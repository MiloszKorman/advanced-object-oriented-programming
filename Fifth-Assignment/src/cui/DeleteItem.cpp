//
// Created by Miłosz Korman on 14.12.18.
//

#include <iostream>
#include "DeleteItem.h"
#include "../../lib/utils/Utils.h"


DeleteItem::DeleteItem(KnapsackProblemHandler *handler) : KnapsackProblemCommand(handler) {}

void DeleteItem::RunCommand() {
    cout << GET_INDEX << endl;
    int index = Utils::getIntBetween(0, handler->getItems()->size()-1);
    handler->deleteItem(index);
}