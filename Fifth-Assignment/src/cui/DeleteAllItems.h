//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_DELETEALLITEMS_H
#define FIFTH_ASSIGNMENT_DELETEALLITEMS_H


#include "KnapsackProblemCommand.h"

class DeleteAllItems : public KnapsackProblemCommand {
public:
    explicit DeleteAllItems(KnapsackProblemHandler *handler);

    void RunCommand() override;
};


#endif //FIFTH_ASSIGNMENT_DELETEALLITEMS_H
