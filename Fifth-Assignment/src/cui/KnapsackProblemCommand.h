//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_KNAPSACKPROBLEMCOMMAND_H
#define FIFTH_ASSIGNMENT_KNAPSACKPROBLEMCOMMAND_H

#include "../domain/KnapsackProblemHandler.h"
#include "../../lib/console-user-interface/Command.h"


class KnapsackProblemCommand : public Command {
public:
    KnapsackProblemCommand(KnapsackProblemHandler *handler);

protected:
    KnapsackProblemHandler *handler;
};


#endif //FIFTH_ASSIGNMENT_KNAPSACKPROBLEMCOMMAND_H
