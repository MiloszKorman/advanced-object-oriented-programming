//
// Created by Miłosz Korman on 14.12.18.
//

#include "DeleteAllItems.h"

DeleteAllItems::DeleteAllItems(KnapsackProblemHandler *handler) : KnapsackProblemCommand(handler) {}

void DeleteAllItems::RunCommand() {
    handler->deleteAllItems();
}
