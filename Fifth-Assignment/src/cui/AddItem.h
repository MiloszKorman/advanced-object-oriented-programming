//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_ADDITEM_H
#define FIFTH_ASSIGNMENT_ADDITEM_H


#define GET_ITEMS_NAME "Items name: "

#define GET_ITEMS_WEIGHT "Items weight: "

#define GET_ITEMS_VALUE "Items value: "

#include "KnapsackProblemCommand.h"

class AddItem : public KnapsackProblemCommand {
public:
    explicit AddItem(KnapsackProblemHandler *handler);

    void RunCommand() override;

private:
    Item *newItem();

};

#endif //FIFTH_ASSIGNMENT_ADDITEM_H
