//
// Created by Miłosz Korman on 14.12.18.
//

#include <iostream>
#include <tuple>
#include "SolveProblem.h"
#include "../../lib/utils/Utils.h"

#define VALUE_SUM_STR "Value sum: "
#define WEIGHT_SUM_STR "Weight sum: "
#define GET_POPULATION_SIZE "What do you want the population size to be?: "
#define GET_TIME "How long (in s) do you want it to run?: "
#define GET_BAG_CAPACITY "How big do you want the bag to be?: "
#define GET_MUTATION_PROBABILITY "How high do you want mutation probability to be?: "
#define GET_CROSS_PROBABILITY "How high do you want cross probability to be?: "

template<typename T>
SolveProblem<T>::SolveProblem(KnapsackProblemHandler *handler) : KnapsackProblemCommand(handler) {}

template<typename T>
void SolveProblem<T>::RunCommand() {
    cout << GET_BAG_CAPACITY << endl;
    double capacity = Utils::getDoubleFrom(1.0);

    cout << GET_POPULATION_SIZE << endl;
    int popSize = Utils::getIntBetween(1, 100);

    cout << GET_MUTATION_PROBABILITY << endl;
    double mutProb = Utils::getDoubleBetween(0.0, 1.0);

    cout << GET_CROSS_PROBABILITY << endl;
    double crossProb = Utils::getDoubleBetween(0.0, 1.0);

    vector<Item *> *items = handler->getItems();

    KnapsackProblem<T> knapsackProblem(*items, capacity);
    GeneticAlgorithm<T> geneticAlgorithm(popSize, crossProb, mutProb, knapsackProblem);

    cout << GET_TIME << endl;
    int time = Utils::getIntBetween(1, 60);

    Individual<T> *best = geneticAlgorithm.solve(time);
    vector<tuple<T, Item *>> *solution = knapsackProblem.solution(best->getGenotype());

    printSolution(solution);

    delete solution;
    delete best;
}

template<>
void SolveProblem<bool>::printSolution(vector<tuple<bool, Item *>> *solution) {
    int valueSum = 0;
    int weightSum = 0;

    cout << "\n\n" << endl;

    for (tuple<bool, Item *> tup : *solution) {
        if (get<0>(tup)) {
            cout << *get<1>(tup) << endl;
            valueSum += get<1>(tup)->getValue();
            weightSum += get<1>(tup)->getWeight();
        }
    }

    cout << VALUE_SUM_STR << valueSum << endl;
    cout << WEIGHT_SUM_STR << weightSum << endl;
}

template<>
void SolveProblem<int>::printSolution(vector<tuple<int, Item *>> *solution) {
    int valueSum = 0;
    int weightSum = 0;

    cout << "\n\n" << endl;

    for (tuple<int, Item *> tup : *solution) {
        if (get<0>(tup) != 0) {
            cout << get<0>(tup) << " ";
            cout << *get<1>(tup) << endl;
            valueSum += get<0>(tup) * get<1>(tup)->getValue();
            weightSum += get<0>(tup) * get<1>(tup)->getWeight();
        }
    }

    cout << VALUE_SUM_STR << valueSum << endl;
    cout << WEIGHT_SUM_STR << weightSum << endl;
}

template<>
void SolveProblem<double>::printSolution(vector<tuple<double, Item *>> *solution) {
    double valueSum = 0.0;
    double weightSum = 0.0;

    cout << "\n\n" << endl;

    for (tuple<double, Item *> tup : *solution) {
        if (get<0>(tup) != 0) {
            cout << get<0>(tup) << " ";
            cout << *get<1>(tup) << endl;
            valueSum += get<0>(tup) * get<1>(tup)->getValue();
            weightSum += get<0>(tup) * get<1>(tup)->getWeight();
        }
    }

    cout << VALUE_SUM_STR << valueSum << endl;
    cout << WEIGHT_SUM_STR << weightSum << endl;
}

