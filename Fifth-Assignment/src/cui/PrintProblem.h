//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_PRINTPROBLEM_H
#define FIFTH_ASSIGNMENT_PRINTPROBLEM_H


#include "KnapsackProblemCommand.h"


class PrintProblem : public KnapsackProblemCommand {
public:
    explicit PrintProblem(KnapsackProblemHandler *handler);

    void RunCommand() override;

};

#endif //FIFTH_ASSIGNMENT_PRINTPROBLEM_H
