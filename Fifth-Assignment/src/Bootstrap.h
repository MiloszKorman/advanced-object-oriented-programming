//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_BOOTSTRAP_H
#define FIFTH_ASSIGNMENT_BOOTSTRAP_H


#include "../lib/console-user-interface/Menu.h"
#include "domain/KnapsackProblemHandler.h"

class Bootstrap {
public:
    void setMainMenu(Menu &mainMenu, KnapsackProblemHandler &handler);
};


#endif //FIFTH_ASSIGNMENT_BOOTSTRAP_H
