#include <iostream>
#include <vector>
#include "../lib/knapsack_problem/KnapsackProblem.h"
#include "../lib/knapsack_problem/genetic_knapsack_algorithm/Individual.h"
#include "../lib/knapsack_problem/genetic_knapsack_algorithm/GeneticAlgorithm.h"
#include "domain/KnapsackProblemHandler.h"
#include "../lib/console-user-interface/Menu.h"
#include "Bootstrap.h"

using namespace std;


int main() {
    /*vector<Item *> items{
            new Item("item 1", 10, 7),
            new Item("item 2", 7, 5),
            new Item("item 3", 5, 4),
            new Item("item 4", 3, 3),
            new Item("item 5", 6, 6),
            new Item("item 6", 8, 7),
            new Item("item 7", 3, 2),
            new Item("item 8", 2, 1)
    };

    //bool
    *//*KnapsackProblem<bool> knapsackProblem(items, 25);
    GeneticAlgorithm<bool> geneticAlgorithm(20, 0.75, 0.25, knapsackProblem);

    Individual<bool> *best = geneticAlgorithm.solve(1);
    vector<tuple<bool, Item *>> *solution = knapsackProblem.solution(best->getGenotype());*//*

    //int
    *//*KnapsackProblem<int> knapsackProblem(items, 25);
    GeneticAlgorithm<int> geneticAlgorithm(20, 0.75, 0.25, knapsackProblem);

    Individual<int> *best = geneticAlgorithm.solve(1);
    vector<tuple<int, Item *>> *solution = knapsackProblem.solution(best->getGenotype());*//*

    //double
    KnapsackProblem<double> knapsackProblem(items, 25);
    GeneticAlgorithm<double> geneticAlgorithm(20, 0.75, 0.25, knapsackProblem);

    Individual<double> *best = geneticAlgorithm.solve(1);
    vector<tuple<double, Item *>> *solution = knapsackProblem.solution(best->getGenotype());

    cout << "Solution: " << endl;
    //Expected: 3 4 5 6 7 ? (bool: valueSum=22 weightSum=25) (int: valueSum=24 weightSum=24) (double: valueSum=25 weightSum=25)
    //bool//int
    *//*int valueSum = 0;
    int weightSum = 0;*//*

    //double
    double valueSum = 0.0;
    double weightSum = 0.0;

    //bool
    *//*for (tuple<bool, Item *> tup : *solution) {
        if (get<0>(tup)) {
            cout << *get<1>(tup) << endl;
            valueSum += get<1>(tup)->getValue();
            weightSum += get<1>(tup)->getWeight();
        }
    }*//*

    //int
    *//*for (tuple<int, Item *> tup : *solution) {
        if (get<0>(tup) != 0) {
            cout << get<0>(tup) << " ";
            cout << *get<1>(tup) << endl;
            valueSum += get<0>(tup) * get<1>(tup)->getValue();
            weightSum += get<0>(tup) * get<1>(tup)->getWeight();
        }
    }*//*

    //double
    for (tuple<double, Item *> tup : *solution) {
        if (get<0>(tup) != 0) {
            cout << get<0>(tup) << " ";
            cout << *get<1>(tup) << endl;
            valueSum += get<0>(tup) * get<1>(tup)->getValue();
            weightSum += get<0>(tup) * get<1>(tup)->getWeight();
        }
    }

    cout << "Value sum: " << valueSum << endl;
    cout << "Weight sum: " << weightSum << endl;

    for (Item *item : items) {
        delete item;
    }

    //cout << "time in mili: " << t << endl;

    delete solution;
    delete best;*/

    /*KnapsackProblemHandler<bool> handler;
    handler.addItem(new Item("item 1", 10, 7));
    handler.addItem(new Item("item 2", 7, 5));
    handler.addItem(new Item("item 3", 5, 4));
    handler.addItem(new Item("item 4", 3, 3));
    handler.addItem(new Item("item 5", 6, 6));
    handler.addItem(new Item("item 6", 8, 7));
    handler.addItem(new Item("item 7", 3, 2));
    handler.addItem(new Item("item 8", 2, 1));
    handler.printItems();
    handler.solveProblem();*/

    Menu mainMenu("menu", "Main menu");
    KnapsackProblemHandler handler;
    Bootstrap bootstrap;
    bootstrap.setMainMenu(mainMenu, handler);
    mainMenu.Run();

    return 0;
}
