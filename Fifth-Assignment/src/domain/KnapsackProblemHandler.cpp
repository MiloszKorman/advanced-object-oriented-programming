//
// Created by Miłosz Korman on 14.12.18.
//

#include <iostream>
#include <tuple>
#include "KnapsackProblemHandler.h"
#include "../../lib/knapsack_problem/KnapsackProblem.h"
#include "../../lib/knapsack_problem/genetic_knapsack_algorithm/GeneticAlgorithm.h"

using namespace std;

KnapsackProblemHandler::KnapsackProblemHandler() {
    items = new vector<Item *>();
}

KnapsackProblemHandler::~KnapsackProblemHandler() {
    for (Item *item : *items) {
        delete item;
    }
    delete items;
}

void KnapsackProblemHandler::addItem(Item *toAdd) {
    items->push_back(toAdd);
}

void KnapsackProblemHandler::deleteItem(int index) {
    delete items->at(index);
    items->erase(items->begin() + index);
}

void KnapsackProblemHandler::deleteAllItems() {
    for (Item *item : *items) {
        delete item;
    }
    items->clear();
}

void KnapsackProblemHandler::printItems() {
    for (int i = 0; i < items->size(); i++) {
        cout << i << " " << *items->at(i) << endl;
    }
}

std::vector<Item *> *KnapsackProblemHandler::getItems() {
    return items;
}
