//
// Created by Miłosz Korman on 14.12.18.
//

#ifndef FIFTH_ASSIGNMENT_KNAPSACKPROBLEMHANDLER_H
#define FIFTH_ASSIGNMENT_KNAPSACKPROBLEMHANDLER_H


#include <vector>
#include "../../lib/knapsack_problem/Item.h"
#include "../../lib/knapsack_problem/KnapsackProblem.h"
#include "../../lib/knapsack_problem/genetic_knapsack_algorithm/GeneticAlgorithm.h"

using namespace std;

class KnapsackProblemHandler {
public:
    KnapsackProblemHandler();

    ~KnapsackProblemHandler();

    void addItem(Item *toAdd);

    void deleteItem(int index);

    void deleteAllItems();

    std::vector<Item *> *getItems();

    void printItems();

private:
    std::vector<Item *> *items;
};


#endif //FIFTH_ASSIGNMENT_KNAPSACKPROBLEMHANDLER_H
