//
// Created by Miłosz Korman on 14.12.18.
//

#include "Bootstrap.h"
#include "cui/AddItem.h"
#include "cui/PrintProblem.h"
#include "cui/DeleteItem.h"
#include "cui/SolveProblem.h"
#include "cui/DeleteAllItems.h"
#include "../lib/console-user-interface/MenuCommand.h"

void Bootstrap::setMainMenu(Menu &mainMenu, KnapsackProblemHandler &handler) {
    mainMenu.addItem(new MenuCommand("add", "Add Item to knapsack", new AddItem(&handler)));
    mainMenu.addItem(new MenuCommand("delete", "Delete Item from knapsack", new DeleteItem(&handler)));
    mainMenu.addItem(new MenuCommand("delete all", "Delete All Items from knapsack", new DeleteAllItems(&handler)));
    mainMenu.addItem(new MenuCommand("print", "Print items in knapsack", new PrintProblem(&handler)));
    mainMenu.addItem(new MenuCommand("bool", "Boolean solution", new SolveProblem<bool>(&handler)));
    mainMenu.addItem(new MenuCommand("int", "Integer solution", new SolveProblem<int>(&handler)));
    mainMenu.addItem(new MenuCommand("double", "Real numbers solution", new SolveProblem<double>(&handler)));
}
