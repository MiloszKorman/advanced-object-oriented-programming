## 1

```
Wrocław 2018.10.
Autorzy: Michał Przewoźniczek, Marcin Komarnicki
```
```
Zaawansowane Metody Programowania Obiektowego – zadanie 5
Użycie obiektów, duża liczba alokowanych i dealokowanych obiektów, szablony
operatory, operacje na plikach
```
## UWAGA:

1. Pisząc własny program można użyć innego nazewnictwa niż to przedstawione w
    treści zadania. Należy jednak użyć jakiejś spójnej konwencji kodowania, zgodnie z
    wymaganiami kursu.
2. Program NALEŻY NAPISAĆ OBIEKTOWO.
3. Ocenie podlega jakość i prawidłowość działania programu. Ocenie nie podlega
    jakość optymalizacji.

```
Należy rozwinąć program napisany w ramach zadania 4 o w następujący sposób:
```
1. W zadaniu nr 4 rozważano binarny problem plecakowy. Teraz do programu należy
    wprowadzić szablony (dostępne możliwości bool, int i double).
       - Bool, wtedy mamy do czynienia z binarnym problemem plecakowym (jak w zadaniu
          nr 4).
       - Int, wtedy mamy do czynienia z problemem plecakowym, gdzie każdy przedmiot
          może zostać wybrany wiele razy (jest to tzw. integer knapsack problem ), czyli
          niektórych przedmiotów możemy nie brać wcale, przedmiotu nr 4 bierzemy jedną
          sztukę, a przedmiotu nr 6 bierzemy 4 sztuki.
       - Double, wtedy mamy każdy przedmiot możemy wziąć w takiej ilości, jaka nam
          odpowiada, czyli można np. wziąć 4.34 przedmiotu.
2. Mechanizm szablonu musi zostać wprowadzony do wszystkich wymaganych klas, czyli:
    CKnapsackProblem, CGeneticAlgorithm, CIndividual.
3. Dla operacji mutacji należy przeciążyć operator „++”, a więc wykonanie operacji:
    c_individual++;
    gdzie c_individual jest obiektem klasy CIndividual, będzie wykonywać operację mutacji
    danego osobnika.
4. Dla operacji krzyżowania, należy przeciążyć operator „+”. Operator + ma zwracać
    pierwsze dziecko, które jest wynikiem krzyżowania, a więc po wykonaniu operacji:
       c_child = c_mummy + c_daddy;
       gdzie c_child, c_mummy i c_daddy to obiekty klasy CIndividual, obiekt c_child będzie
       pierwszym potomkiem wynikającym z krzyżowania obiektów c_mummy i c_daddy.
5. Jako kryterium zatrzymania metody należy przyjąć czas obliczeń podawany w
    sekundach, zamiast liczby iteracji jak w zadaniu nr 4.
6. Program ma posiadać interfejs użytkownika, wykonany przy użyciu klas napisanych
    w ramach zadania nr 2.
