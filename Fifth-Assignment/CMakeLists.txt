cmake_minimum_required(VERSION 3.12)
project(Fifth_Assignment)

set(CMAKE_CXX_STANDARD 14)

add_executable(Fifth_Assignment
        src/main.cpp
        src/Bootstrap.cpp
        src/domain/KnapsackProblemHandler.cpp
        src/cui/KnapsackProblemCommand.cpp
        src/cui/AddItem.cpp
        src/cui/DeleteItem.cpp
        src/cui/DeleteAllItems.cpp
        src/cui/PrintProblem.cpp
        src/cui/SolveProblem.h
        lib/utils/Utils.cpp
        lib/knapsack_problem/KnapsackProblem.h
        lib/knapsack_problem/KnapsackProblem.cpp
        lib/knapsack_problem/Item.cpp
        lib/knapsack_problem/genetic_knapsack_algorithm/Individual.h
        lib/knapsack_problem/genetic_knapsack_algorithm/Individual.cpp
        lib/knapsack_problem/genetic_knapsack_algorithm/GeneticAlgorithm.h
        lib/console-user-interface/MenuItem.cpp
        lib/console-user-interface/MenuCommand.cpp
        lib/console-user-interface/Menu.cpp
        lib/console-user-interface/Command.cpp)