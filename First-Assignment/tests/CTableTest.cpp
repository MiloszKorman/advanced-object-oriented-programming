//
// Created by Miłosz Korman on 06.10.18.
//

#define CATCH_CONFIG_MAIN
#include "../lib/catch.hpp"
#include "../src/CTable.h"

SCENARIO("Test constructor")
{
    GIVEN("CTable object")
    {
        WHEN("default constructor is called")
        {
            CTable table;

            THEN("objects fields should have appropriate values")
            {
                REQUIRE(table.sGetName() == "Best Table");
                REQUIRE(table.iGetLength() == 10);
            }
        }

        WHEN("parameter constructor is called")
        {
            CTable table("MyTable", 12);

            THEN("objects fields should have appropriate values")
            {
                REQUIRE(table.sGetName() == "MyTable");
                REQUIRE(table.iGetLength() == 12);
            }
        }

        AND_GIVEN("Another CTable object")
        {
            CTable cpyThis("Table", 3);
            REQUIRE(cpyThis.bSet(0, 1));
            REQUIRE(cpyThis.bSet(1, 2));
            REQUIRE(cpyThis.bSet(2, 3));

            WHEN("copying constructor is called")
            {
                CTable table(cpyThis);

                THEN("objects fields should have appropriate values")
                {
                    REQUIRE(table.sGetName() == "Table_copy");
                    REQUIRE(table.iGetLength() == 3);

                    bool success = false;
                    REQUIRE(table.iGet(0, &success) == 1);
                    REQUIRE(success);
                    REQUIRE(table.iGet(1, &success) == 2);
                    REQUIRE(success);
                    REQUIRE(table.iGet(2, &success) == 3);
                    REQUIRE(success);
                }
            }
        }
    }
}

SCENARIO("Test set name")
{
    GIVEN("CTable object")
    {
        CTable table;

        WHEN("It's name is changed")
        {
            table.vSetName("MyTable");

            THEN("It should change")
            {
                REQUIRE(table.sGetName() == "MyTable");
            }
        }
    }
}

SCENARIO("Test set length")
{
    GIVEN("CTable object")
    {
        CTable table("Whatever", 5);

        WHEN("it's fields are set")
        {
            REQUIRE(table.bSet(0, 1));
            REQUIRE(table.bSet(1, 2));
            REQUIRE(table.bSet(2, 3));
            REQUIRE(table.bSet(3, 4));
            REQUIRE(table.bSet(4, 5));

            AND_WHEN("it's size is expanded")
            {
                REQUIRE(table.bSetLength(10));

                GIVEN("success variable")
                {
                    bool success = false;

                    THEN("check new fields are 0")
                    {
                        REQUIRE(table.iGet(5, &success) == 0);
                        REQUIRE(success);
                        REQUIRE(table.iGet(6, &success) == 0);
                        REQUIRE(success);
                        REQUIRE(table.iGet(7, &success) == 0);
                        REQUIRE(success);
                        REQUIRE(table.iGet(8, &success) == 0);
                        REQUIRE(success);
                        REQUIRE(table.iGet(9, &success) == 0);
                        REQUIRE(success);
                    }

                    AND_WHEN("new fields are changed")
                    {
                        REQUIRE(table.bSet(5, 11));
                        REQUIRE(table.bSet(6, 12));
                        REQUIRE(table.bSet(7, 13));
                        REQUIRE(table.bSet(8, 14));
                        REQUIRE(table.bSet(9, 15));

                        THEN("make sure all fields have proper values")
                        {
                            REQUIRE(table.iGet(0, &success) == 1);
                            REQUIRE(success);
                            REQUIRE(table.iGet(1, &success) == 2);
                            REQUIRE(success);
                            REQUIRE(table.iGet(2, &success) == 3);
                            REQUIRE(success);
                            REQUIRE(table.iGet(3, &success) == 4);
                            REQUIRE(success);
                            REQUIRE(table.iGet(4, &success) == 5);
                            REQUIRE(success);
                            REQUIRE(table.iGet(5, &success) == 11);
                            REQUIRE(success);
                            REQUIRE(table.iGet(6, &success) == 12);
                            REQUIRE(success);
                            REQUIRE(table.iGet(7, &success) == 13);
                            REQUIRE(success);
                            REQUIRE(table.iGet(8, &success) == 14);
                            REQUIRE(success);
                            REQUIRE(table.iGet(9, &success) == 15);
                            REQUIRE(success);
                        }
                    }
                }
            }

            AND_WHEN("it's size is truncated")
            {
                REQUIRE(table.bSetLength(3));

                GIVEN("success variable")
                {
                    bool success = true;

                    THEN("left out fields values shouldn't change")
                    {
                        REQUIRE(table.iGet(0, &success) == 1);
                        REQUIRE(table.iGet(1, &success) == 2);
                        REQUIRE(table.iGet(2, &success) == 3);
                    }

                    AND_WHEN("illegal value is accessed")
                    {
                        REQUIRE(table.iGet(3, &success) == -1);

                        THEN("it shouldn't be successful")
                        {
                            REQUIRE_FALSE(success);
                        }
                    }
                }
            }

            AND_WHEN("unreasonable argument is passed to set length method")
            {
                REQUIRE_FALSE(table.bSetLength(-15));

                THEN("nothing should change")
                {
                    bool success = false;
                    REQUIRE(table.iGet(0, &success) == 1);
                    REQUIRE(success);
                    REQUIRE(table.iGet(1, &success) == 2);
                    REQUIRE(success);
                    REQUIRE(table.iGet(2, &success) == 3);
                    REQUIRE(success);
                    REQUIRE(table.iGet(3, &success) == 4);
                    REQUIRE(success);
                    REQUIRE(table.iGet(4, &success) == 5);
                    REQUIRE(success);
                }
            }
        }
    }
}

SCENARIO("Test accessing elements")
{
    GIVEN("CTable object and variable indicating success")
    {
        CTable table;
        bool success = false;

        WHEN("It's elements are changed")
        {
            AND_WHEN("They are within legal range")
            {
                THEN("They should change")
                {
                    REQUIRE(table.bSet(0, 15));
                    REQUIRE(table.iGet(0, &success) == 15);
                    REQUIRE(success);
                    REQUIRE(table.bSet(0, 16));
                    REQUIRE(table.iGet(0, &success) == 16);
                    REQUIRE(success);
                    REQUIRE(table.bSet(9, 9));
                    REQUIRE(table.iGet(9, &success) == 9);
                    REQUIRE(success);
                }
            }

            AND_WHEN("They aren't within legal range")
            {
                THEN("They shouldn't change")
                {
                    REQUIRE_FALSE(table.bSet(-12, 14));
                    REQUIRE(table.iGet(-12, &success) == -1);
                    REQUIRE_FALSE(success);
                }
            }
        }
    }
}

SCENARIO("Test poClone")
{
    GIVEN("CTable object")
    {
        CTable table("Name", 3);
        REQUIRE(table.bSet(0, 1));
        REQUIRE(table.bSet(1, 2));
        REQUIRE(table.bSet(2, 3));

        WHEN("It's cloned")
        {
            CTable* cloned = table.poClone();

            THEN("Those objects should be in different places in memory")
            {
                REQUIRE_FALSE(cloned == &table);
            }

            THEN("Should have all fields same")
            {
                bool success = true;
                REQUIRE(table.sGetName() + "_copy" == cloned->sGetName());
                REQUIRE(table.iGetLength() == cloned->iGetLength());
                REQUIRE(table.iGet(0, &success) == cloned->iGet(0, &success));
                REQUIRE(table.iGet(1, &success) == cloned->iGet(1, &success));
                REQUIRE(table.iGet(2, &success) == cloned->iGet(2, &success));
            }

            delete cloned; //why do I need to specify this? because it's stored as a pointer?
        }
    }
}

SCENARIO("Test operator =")
{
    GIVEN("CTable object to be copied")
    {
        CTable toCpy("To copy", 3);
        REQUIRE(toCpy.bSet(0, 1));
        REQUIRE(toCpy.bSet(1, 2));
        REQUIRE(toCpy.bSet(2, 3));

        AND_GIVEN("CTable object copying")
        {
            CTable table("Copying", 6);
            REQUIRE(table.bSet(0, 6));
            REQUIRE(table.bSet(1, 5));
            REQUIRE(table.bSet(2, 4));
            REQUIRE(table.bSet(3, 3));
            REQUIRE(table.bSet(4, 2));
            REQUIRE(table.bSet(5, 1));

            WHEN("Using = operator")
            {
                table = toCpy;

                THEN("CTables objects array changes")
                {
                    REQUIRE(table.sGetName() == "Copying");
                    REQUIRE(table.iGetLength() == 3);

                    bool success = true;
                    REQUIRE(table.iGet(0, &success) == 1);
                    REQUIRE(table.iGet(1, &success) == 2);
                    REQUIRE(table.iGet(2, &success) == 3);
                }
            }
        }
    }
}

SCENARIO("Test sToString")
{
    GIVEN("CTable object")
    {
        CTable table("Name", 3);

        WHEN("It's values are set")
        {
            table.bSet(0, 1);
            table.bSet(1, 2);
            table.bSet(2, 3);

            THEN("It's sToString should show everything properly")
            {
                REQUIRE(table.sToString() == "Name len: 3 values: 1, 2, 3");
            }
        }
    }
}

SCENARIO("Test double array")
{
    GIVEN("CTable object")
    {
        CTable table("Name", 3);
        
        WHEN("It's values are set")
        {
            table.bSet(0, 5);
            table.bSet(1, 3);
            table.bSet(2, 0);
            
            THEN("doubleArr should double array size and copy array elements")
            {
                bool success;
                table.doubleArr();
                REQUIRE(table.iGetLength() == 6);
                REQUIRE(table.iGet(0, &success) == 5);
                REQUIRE(table.iGet(1, &success) == 3);
                REQUIRE(table.iGet(2, &success) == 0);
                REQUIRE(table.iGet(3, &success) == 5);
                REQUIRE(success);
                REQUIRE(table.iGet(4, &success) == 3);
                REQUIRE(success);
                REQUIRE(table.iGet(5, &success) == 0);
                REQUIRE(success);
            }
        }
    }
}
