//
// Created by Miłosz Korman on 07.10.18.
//

#include "../lib/catch.hpp"
#include "../src/Utils.h"

SCENARIO("Test zeroing an array")
{
   GIVEN("an array")
   {
       int* arr;
       arr = new int [3];
       AND_GIVEN("values in the array")
       {
           arr[0] = 15;
           arr[1] = 13;
           arr[2] = 2;

           WHEN("we call zeroing method on the array")
           {
               REQUIRE(Utils::bPopulateArrayWithZeros(arr, 0, 3));

               THEN("all fields should be zero")
               {
                   REQUIRE(arr[0] == 0);
                   REQUIRE(arr[1] == 0);
                   REQUIRE(arr[2] == 0);
               }
           }

           WHEN("we call zeroing method on the array - with negative number as start")
           {
               REQUIRE_FALSE(Utils::bPopulateArrayWithZeros(arr, -15, 4));

               THEN("it shouldn't do anything")
               {
                   REQUIRE(arr[0] == 15);
                   REQUIRE(arr[1] == 13);
                   REQUIRE(arr[2] == 2);
               }
           }
       }
   }
}
