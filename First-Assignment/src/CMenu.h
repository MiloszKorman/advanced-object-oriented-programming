//
// Created by Miłosz Korman on 08.10.18.
//

#ifndef FIRST_ASSIGNMENT_CMENU_H
#define FIRST_ASSIGNMENT_CMENU_H

#include "vector"
#include <iostream>
#include "CTable.h"
#include "Utils.h"

#define SUPPLY_PARAMETERS "Do you want to supply parameters?: "
#define SUPPLY_INDEX "Supply index"
#define EMPTY_LIST "Can't operate on empty list"

#define ADD_SING_CTABLE 1
#define ADD_MULTI_CTABLE 2
#define CLONE_CTABLE 3
#define DESTROY_SING_CTABLE 4
#define DESTROY_ALL_CTABLE 5
#define CHANGE_CTABLE_SIZE 6
#define CHANGE_CTABLE_NAME 7
#define CHANGE_VAL 8
#define PRINT_CTABLE 9
#define PRINT_ALL 10
#define EXIT 11

class CMenu {
public:
    //Constructors
    CMenu();

    //Destructor
    ~CMenu();

    //Methods
    void vRun();

private:
    //Variables
    std::vector<CTable*> v_tables;

    //Methods
    void v_print_menu();
    void v_add_multiple_c_tables(int num, bool parameterized);
    void v_add_c_table();
    void v_add_c_table_parameterized();
    void v_destroy_c_table(int index);
    void v_destroy_all();
    void v_change_c_table_name(int index);
    void v_change_c_table_length(int index);
    void v_clone_c_table(int index);
    void v_print_c_table(int index);
    void v_print_all();
    void v_change_value_in_c_table(int index);
    int i_get_max();
};


#endif //FIRST_ASSIGNMENT_CMENU_H
