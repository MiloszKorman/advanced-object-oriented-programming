//
// Created by Miłosz Korman on 08.10.18.
//

#include "CMenu.h"

/*
 * Constructors
 */
CMenu::CMenu()
{

}


/*
 * Destructors
 */
CMenu::~CMenu()
{
    v_destroy_all();
}


/*
 * Public methods
 */
void CMenu::vRun()
{
    v_print_menu();

    int usersChoice;
    bool choice;

    do {
        std::cout << "What do you want to do?: ";
        usersChoice = Utils::iGetIntBetween(ADD_SING_CTABLE, EXIT);

        switch (usersChoice)
        {
            case ADD_SING_CTABLE:
                std::cout << SUPPLY_PARAMETERS;
                if (Utils::bGetBoolean()) v_add_c_table_parameterized();
                else v_add_c_table();
                break;
            case ADD_MULTI_CTABLE:
                std::cout << SUPPLY_PARAMETERS;
                choice = Utils::bGetBoolean();
                std::cout << "How many?" << std::endl;
                v_add_multiple_c_tables(
                        Utils::iGetIntFrom(1),
                        choice);
                break;
            case CLONE_CTABLE:
                if (v_tables.empty()) std::cout << EMPTY_LIST << std::endl;
                else
                {
                    std::cout << SUPPLY_INDEX << std::endl;
                    v_clone_c_table(Utils::iGetIntBetween(0, i_get_max()));
                }
                break;
            case DESTROY_SING_CTABLE:
                if (v_tables.empty()) std::cout << EMPTY_LIST << std::endl;
                else
                {
                    std::cout << SUPPLY_INDEX << std::endl;
                    v_destroy_c_table(Utils::iGetIntBetween(0, i_get_max()));
                }
                break;
            case DESTROY_ALL_CTABLE:
                v_destroy_all();
                break;
            case CHANGE_CTABLE_SIZE:
                if (v_tables.empty()) std::cout << EMPTY_LIST << std::endl;
                else
                {
                    std::cout << SUPPLY_INDEX << std::endl;
                    v_change_c_table_length(Utils::iGetIntBetween(0, i_get_max()));
                }
                break;
            case CHANGE_CTABLE_NAME:
                if (v_tables.empty()) std::cout << EMPTY_LIST << std::endl;
                else
                {
                    std::cout << SUPPLY_INDEX << std::endl;
                    v_change_c_table_name(Utils::iGetIntBetween(0, i_get_max()));
                }
                break;
            case CHANGE_VAL:
                if (v_tables.empty()) std::cout << EMPTY_LIST << std::endl;
                else
                {
                    std::cout << SUPPLY_INDEX << std::endl;
                    v_change_value_in_c_table(Utils::iGetIntBetween(0, i_get_max()));
                }
                break;
            case PRINT_CTABLE:
                if (v_tables.empty()) std::cout << EMPTY_LIST << std::endl;
                else
                {
                    std::cout << SUPPLY_INDEX << std::endl;
                    v_print_c_table(Utils::iGetIntBetween(0, i_get_max()));
                }
                break;
            case PRINT_ALL:
                v_print_all();
                break;
            default:
                std::cout << "Bye bye ;)" << std::endl;
                usersChoice = EXIT;
        }

    } while (usersChoice != EXIT);
}


/*
 * Private methods
 */
void CMenu::v_print_menu()
{
    std::cout << "----------------------------Menu----------------------------" << "\n"
              << "1. Create and add single CTable object." << "\n"
              << "2. Create and add multiple CTable objects." << "\n"
              << "3. Add clone of CTable object" << "\n"
              << "4. Delete single CTable object." << "\n"
              << "5. Delete all CTable objects." << "\n"
              << "6. Change specific CTable objects size." << "\n"
              << "7. Change specific CTable objects name." << "\n"
              << "8. Change specific fields content in specific CTable object." << "\n"
              << "9. Print specific CTable object." << "\n"
              << "10. Print all CTable objects." << "\n"
              << "11. End Program." << "\n" << std::endl;
}

void CMenu::v_add_multiple_c_tables(int num, bool parameterized)
{
    for (int i = 0; i < num; i++)
    {
        if (parameterized) v_add_c_table_parameterized();
        else v_add_c_table();
    }
}

void CMenu::v_add_c_table()
{
    v_tables.push_back(new CTable());
}

void CMenu::v_add_c_table_parameterized()
{
    std::cout << "What do you want to name it?: ";
    std::string name = Utils::sGetString();
    std::cout << "How many fields should it have?: " << std::endl;
    int nOfFields = Utils::iGetIntFrom(1);
    v_tables.push_back(new CTable(name, nOfFields));
}

void CMenu::v_destroy_c_table(int index)
{
    delete v_tables[index];
    v_tables.erase(v_tables.begin() + index);
}

void CMenu::v_destroy_all()
{
    for (int i = 0; i < v_tables.size(); i++)
        delete v_tables[i];

    v_tables.clear();
}

void CMenu::v_change_c_table_name(int index)
{
    std::cout << "What the new name should be?: ";
    std::string newName = Utils::sGetString();
    v_tables[index]->vSetName(newName);
}

void CMenu::v_change_c_table_length(int index)
{
    std::cout << "What the new length should be?: " << std::endl;
    int newLength = Utils::iGetIntFrom(1);
    v_tables[index]->bSetLength(newLength);
}

void CMenu::v_clone_c_table(int index)
{
    v_tables.push_back(v_tables[index]->poClone());
}

void CMenu::v_print_c_table(int index)
{
    std::cout << index << ":\t" << (v_tables.at(index))->sToString() << std::endl;
}

void CMenu::v_print_all()
{
    for (int i = 0; i < v_tables.size(); i++)
        v_print_c_table(i);

}

void CMenu::v_change_value_in_c_table(int index)
{
    std::cout << "At which place?: " << std::endl;
    int valueIndex = Utils::iGetIntBetween(0, v_tables[index]->iGetLength() - 1);
    std::cout << "What the new value should be?: " << std::endl;
    int newValue = Utils::iGetAnyInt();
    v_tables[index]->bSet(valueIndex, newValue);
}

int CMenu::i_get_max()
{
    return (v_tables.size() - 1 > INT32_MAX) ? INT32_MAX : (int) v_tables.size() - 1;
}
