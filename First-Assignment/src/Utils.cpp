//
// Created by Miłosz Korman on 07.10.18.
//

#include "Utils.h"

bool Utils::bCheckRange(int index, int start, int finish)
{
    return (start <= index && index < finish);
}

bool Utils::bPopulateArrayWithZeros(int *array, int start, int finish)
{
    if (0 <= start && start < finish)
    {
        for (int i = start; i < finish; i++)
        {
            array[i] = 0;
        }

        return true;
    } else return false;
}

//harrow user until valid y/n answer is given
bool Utils::bGetBoolean()
{
    std::string response;
    char key;
    do {
        std::cout << "(Y/N)?: ";
        std::cin >> response;
        key = response.at(0);

        if (key == 'Y' || key == 'y') return true;
        if (key == 'N' || key == 'n') return false;

    } while (key == 'Y' || key == 'y' || key == 'N' || key == 'n');

    return false; //should never come here
}

//harrow user until valid int answer is given
int Utils::iGetIntBetween(int bottom, int top)
{
    std::cout << "Provide an integer between " << bottom << " and " << top << ": ";

    int response = bottom - 1;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger || response < bottom || top < response);

    return response;
}

//harrow user until valid int answer is given
int Utils::iGetIntFrom(int bottom)
{
    std::cout << "Provide an integer equal or greater than " << bottom << ": ";

    int response = bottom - 1;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger || response < bottom);

    return response;
}

//harrow user until valid int answer is given
int Utils::iGetAnyInt()
{
    std::cout << "Provide an integer: ";

    int response;
    bool properInteger;
    do {
        std::string string;
        std::stringstream stream;
        std::getline(std::cin, string);
        stream << string;
        properInteger = (bool) (stream >> response);
    } while (!properInteger);

    return response;
}

std::string Utils::sGetString()
{
    std::string response;
    while (response.empty())
        std::getline(std::cin, response);

    return response;
}