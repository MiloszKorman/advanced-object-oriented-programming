//
// Created by Miłosz Korman on 06.10.18.
//

#include "CTable.h"

/*
 * Constructors
 */
CTable::CTable() : I_DEFAULT_SIZE(10), S_DEFAULT_NAME("Best Table"),
                   s_name(S_DEFAULT_NAME), i_table_length(I_DEFAULT_SIZE)
{
    pi_array = new int[i_table_length];
    Utils::bPopulateArrayWithZeros(pi_array, 0, i_table_length);

    std::cout << "bezp: " << s_name << std::endl;
}

CTable::CTable(std::string sName, int iTableLength) : I_DEFAULT_SIZE(10), S_DEFAULT_NAME("Best Table"),
                                                      s_name(sName), i_table_length(iTableLength)
{
    pi_array = new int[i_table_length];
    Utils::bPopulateArrayWithZeros(pi_array, 0, i_table_length);

    std::cout << "parametr: " << s_name << std::endl;
}

CTable::CTable(CTable &pcOther) : I_DEFAULT_SIZE(10), S_DEFAULT_NAME("Best Table"),
                                  s_name(pcOther.s_name + "_copy"), i_table_length(pcOther.i_table_length)
{
    pi_array = new int[i_table_length];

    for (int i = 0; i < i_table_length; i++)
        *(pi_array + i) = *(pcOther.pi_array + i);

    std::cout << "kopiuj: " << s_name << std::endl;
}


/*
 * Destructor
 */
CTable::~CTable()
{
    delete[] pi_array;
    std::cout << "usuwam: " << s_name << std::endl;
}


/*
 * Public Methods
 */
std::string CTable::sGetName()
{
    return s_name;
}

void CTable::vSetName(std::string sName)
{
    s_name = std::move(sName);
}

int CTable::iGetLength()
{
    return i_table_length;
}

bool CTable::bSetLength(int iNewLength)
{
    if (0 <= iNewLength)
    {
        int *newArray;
        newArray = new int[iNewLength];

        int sizeToCopy = (iNewLength < i_table_length) ? iNewLength : i_table_length;
        std::memcpy(newArray, pi_array, sizeof(int) * sizeToCopy); //copy all elements to new array

        delete[] pi_array; //free space from old array
        pi_array = newArray; //assign new array as main array

        Utils::bPopulateArrayWithZeros(pi_array, i_table_length, iNewLength);
        i_table_length = iNewLength; //assign new length as tables length

        return true;
    }
    else return false;
}

//Returns -1 when illegal index is tried to be accessed
int CTable::iGet(int index, bool *success)
{
    if (Utils::bCheckRange(index, 0, i_table_length))
    {
        *success = true;
        return pi_array[index];
    }
    else
    {
        *success = false;
        return -1;
    }
}

bool CTable::bSet(int index, int value)
{
    if (Utils::bCheckRange(index, 0, i_table_length))
    {
        pi_array[index] = value;
        return true;
    }
    else return false;

}

CTable* CTable::poClone()
{
    return new CTable(*this);
}

CTable& CTable::operator=(CTable &pcOther)
{
    i_table_length = pcOther.i_table_length;
    int *newArray = new int[i_table_length];

    for (int i = 0; i < i_table_length; i++)
        *(newArray + i) = *(pcOther.pi_array + i);

    delete[] pi_array;
    pi_array = newArray;

    return *this;
}

std::string CTable::sToString()
{
    std::stringstream objectRepresentation;
    objectRepresentation << s_name;
    objectRepresentation << " len: " << i_table_length;
    objectRepresentation << " values: ";

    for (int i = 0; i < i_table_length; i++)
    {
        objectRepresentation << pi_array[i];

        if (i != i_table_length - 1)
        {
            objectRepresentation << ", ";
        }
    }

    return objectRepresentation.str();
}

void CTable::doubleArr()
{
    int* newTable = new int[i_table_length * 2];
    
    for (int i = 0; i < i_table_length; i++)
    {
        *(newTable + i) = *(pi_array + i);
        *(newTable + i_table_length + i) = *(pi_array + i);
    }
    
    delete [] pi_array;
    pi_array = newTable;
    i_table_length *= 2;
}