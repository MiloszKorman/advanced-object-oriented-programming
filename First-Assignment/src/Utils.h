//
// Created by Miłosz Korman on 07.10.18.
//

#ifndef FIRST_ASSIGNMENT_UTILS_H
#define FIRST_ASSIGNMENT_UTILS_H

#include <string>
#include <iostream>
#include <sstream>

class Utils
{
public:
    static bool bCheckRange(int index, int start, int finish);
    static bool bPopulateArrayWithZeros(int *array, int start, int finish);
    static bool bGetBoolean();
    static int iGetIntBetween(int bottom, int top);
    static std::string sGetString();
    static int iGetIntFrom(int bottom);
    static int iGetAnyInt();
};


#endif //FIRST_ASSIGNMENT_UTILS_H
