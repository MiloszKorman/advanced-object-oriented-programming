//
// Created by Miłosz Korman on 06.10.18.
//

#ifndef FIRST_ASSIGNMENT_CTABLE_H
#define FIRST_ASSIGNMENT_CTABLE_H

#include <string>
#include <iostream>
#include <cstring>
#include <sstream>
#include "Utils.h"

class CTable
{
public:
    //Constructors
    CTable();
    CTable(std::string sName, int iTableLength);
    CTable(CTable &pcOther); //Copying constructor

    //Destructor
    ~CTable();

    //Methods
    std::string sGetName();
    void vSetName(std::string sName);
    int iGetLength();
    bool bSetLength(int iNewLength);
    int iGet(int index, bool *success);
    bool bSet(int index, int value);
    CTable* poClone();
    CTable& operator=(CTable &pcOther);
    std::string sToString();
    void doubleArr();

private:
    //Constants
    const int I_DEFAULT_SIZE;
    const std::string S_DEFAULT_NAME;

    //Variables
    std::string s_name;
    int i_table_length;
    int* pi_array;
};


#endif //FIRST_ASSIGNMENT_CTABLE_H
